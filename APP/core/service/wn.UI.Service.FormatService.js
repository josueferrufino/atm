/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
define(["jquery", "extensions", "pt-service"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.FormatService");

    /**
     * @type {Wincor.UI.Diagnostics.LogProvider|*|Wincor.UI.Diagnostics.LogProvider|*}
     * @private
     */
    const _logger = Wincor.UI.Diagnostics.LogProvider;

    let _localizeService;
    let _configService;

    function addGroupSeparator(nStr, decSep, separator) {
        nStr += '';
        let x = nStr.split(decSep);
        let x1 = x[0];
        let x2 = x.length > 1 ? decSep + x[1] : '';
        let rgx = /(\d+)(\d{3})/;
        while(rgx.test(x1)) {
            x1 = x1.replace(rgx, `$1${separator}$2`);
        }
        return x1 + x2;
    }

    /**
     * This service provides possibilities to format values according to specified format options.
     * @class
     */
    Wincor.UI.Service.FormatService = Class.create(Wincor.UI.Service.PTService/**@lends Wincor.UI.Service.FormatService.prototype*/, {

        /**
         * "FormatService" - the logical name of this service as used in the service-provider
         * @const
         * @type {string}
         */
        NAME: "FormatService",

        /**
         * Contains the current banking context data from the business logic.
         * It is set by {@link module:ProFlex4Op}.
         *
         * The FormatSerive uses the -currencyData attribute, which can look like this:
         * <br><code>
         *     bankingContext.currencyData = { iso: "EUR", text: "Euro", ada: "Euro", symbol: "€", exponent: "-2" }
         * </code>
         */
        bankingContext: {},

        /**
         * The configuration object contains language specific attributes which are necessary for general formatting.
         * To each supported ISO code, there is specific set of assigned parameters, which match the formatting parameters of the Localization configuration in the registry.
         * ```
         * config: {
         *   "de-DE": {
         *       "CurrDecimalSep": ",",
         *       "NumGroupSep": ".",
         *       "CurrServerValueOffset": -2,
         *       "NumDecimalSep": ",",
         *       "NumGrouping": 3,
         *       "CurrPositiveOrder": 1,
         *       "CurrGroupSep": ".",
         *       "CurrNegativeOrder": 8,
         *       "CurrNumDigits": 2,
         *       "NumNegativeOrder": 1,
         *       "NumNumDigits": 2,
         *       "CurrCurrencySymbol": "EUR",
         *       "CurrGrouping": 3,
         *       "NumLeadingZero": 1,
         *       "CurrLeadingZero": 1
         *   },
         *   "en-US": {
         *       "CurrDecimalSep": ".",
         *       "NumGroupSep": ",",
         *       "CurrServerValueOffset": -2,
         *       "NumDecimalSep": ".",
         *       "NumGrouping": 3,
         *       "CurrPositiveOrder": 0,
         *       "CurrGroupSep": ",",
         *       "CurrNegativeOrder": 8,
         *       "CurrNumDigits": 2,
         *       "NumNegativeOrder": 1,
         *       "NumNumDigits": 2,
         *       "CurrCurrencySymbol": "$",
         *       "NumLeadingZero": 1,
         *       "CurrLeadingZero": 1,
         *       "CurrGrouping": 3
         * }
         *   ```
         * @type {object}
         */
        config: {},

        //HINT: during Class.Create own members are not accessable -> instanciate messages/events in constructor !!!

        /**
         * This method formats the given value string depending on the formatOption.
         *
         * At least the format options, which are invoked synchronously, must be implemented in "formatHelper()".
         * But also for asynchronous requests we call "formatHelper()" if the format option is implemented there.
         *
         * @param {string | object} value           The value to be formatted.<br>
         *                                          In case of synchronous format handling (isSynchronous must be true) this argument must be an object
         *                                          in the following format:    <br>
         *                                          <code>{raw: 'unformatted value'}</code>
         * @param {string} formatOption             The formatOption defines how to format the value.
         * @param {function({object})} callback     The callback will get the result as an object:<br>
         *                                          <code> {raw: "unformatted value", result: "formatted value"} </code>
         * @param {boolean=} isSynchronous          True, if the caller needs to handle the format call in a synchronous way.<br>
         *                                          In this case the value argument must be a plain object where the result is turned back afterwards.
         *                                          Please note which formatOption can handle a synchronous format invocation:<br>
         *                                          <ul>
         *                                              <li>'#C' ->      10.00</li>
         *                                              <li>'#M' ->     €10.00</li>
         *                                              <li>'#ATRM0' -> €10</li>
         *                                              <li>'#SSN' -> 123-45-6789</li>
         *                                              <li>'#i' ->      10</li>
         *                                              <li>'#X*' ->     **</li>
         *                                              <li>'#x+-4:4'->  xxxxxx1234</li>
         *                                              <li>'#conditional{'key1':'val1','key2':'val2',...}' -> val1 if value===key1, val2 if value===key2, otherwise unchanged value</li>
         *                                          </ul>
         *                                          All other format options must be handled asynchronous, no matter if "isSynchronous" is true, false or even undefined.
         */
        format: function (value, formatOption, callback, isSynchronous) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> FormatService::format(value=${value}, formatOption='${formatOption}', isSynchronous=${isSynchronous})`);

            if (!isSynchronous && (!callback || typeof callback !== "function")) {
                _logger.error(`FormatService::format received non-function type <${typeof callback}> as callback argument!`);
                return;
            }

            function formatCallback(message) {
                try {
                    callback({result: message.param3, raw: typeof value === 'object' ? value.raw : value});
                }
                catch(exception) {
                    _logger.error("format callback failed " + exception);
                }
            }

            // for the following options we use our own formatter currently...
            if(formatOption &&
                (formatOption === "#C" ||
                 formatOption === "#i" ||
                 formatOption === "#M" ||
                 formatOption === "#ATRM0" ||
                 formatOption === "#SSN" ||
                 formatOption.indexOf("#X") === 0 ||
                 formatOption.indexOf("#x+-") === 0) ||
                 formatOption.indexOf("#conditional") === 0) {

                if(!isSynchronous) {
                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, ". FormatService::format callback call");
                    //Issue 1746190: removed setTimeout (in previous revision), which called the callback. This was not needed and led high performance issues on some systems.
                    callback({result: this.formatHelper(typeof value === 'object' ? value.raw : value, formatOption) });
                }
                else if(typeof value === 'object' && value.raw !== void 0) {
                    value.result = this.formatHelper(value.raw, formatOption);
                }
                else {
                    _logger.error("Wrong format arguments. Please check argument in order to handle format call in a synchronous way.");
                }
            }
            else if(!formatOption) { // case with no format option given
                if(!isSynchronous) {
                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, ". FormatService::format callback call (no format option)");
                    callback({result: typeof value === 'object' ? value.raw : value});
                }
                else if(typeof value === 'object' && value.raw !== void 0) {
                    value.result = value.raw;
                }
                else {
                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, ". FormatService::format Warning: callback call (value is not an object or no 'raw' attribute given)");
                    if (callback) {
                        callback(value);
                    }
                }
            }
            else {
                if(isSynchronous) {
                    _logger.LOG_ANALYSE && _logger.log( _logger.LOG_ANALYSE, `Warning: Argument formatOption contains pattern '${formatOption}' which can not be handled in a synchronous way.`);

                    if(typeof value === 'object' && value.raw !== void 0) {
                        value.result = value.raw;
                    }
                    else {
                        _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, ". FormatService::format Warning: callback call (value is not an object or no 'raw' attribute given)");
                        if (callback) {
                            callback(value);
                        }
                    }

                } else {
                    let expectedLength = value.length < 1024 ? 1024 : value.length * 2;

                    this.FRM_RESOLVE_REQUEST.FWFuncID = 3; //CCFORMAT_FUNC_FORMAT
                    this.FRM_RESOLVE_REQUEST.param1 = formatOption;
                    this.FRM_RESOLVE_REQUEST.meta1 = ["CHAR_ANSI", -1];
                    this.FRM_RESOLVE_REQUEST.param2 = value;
                    this.FRM_RESOLVE_REQUEST.meta2 = ["CHAR_ANSI", -1];
                    this.FRM_RESOLVE_REQUEST.param3 = "";
                    this.FRM_RESOLVE_REQUEST.meta3 = ["CHAR_ANSI", expectedLength];

                    this.FrmResolve(this.FRM_RESOLVE_REQUEST, formatCallback.bind(this));
                }
            }
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< FormatService::format");
        },

        /**
         * Implements various format options. Will be called by "format()" {@link Wincor.UI.Service.FormatService#format}
         * to handle at least _all_ synchronous formatting, but also to handle implemented format options in an asynchronous way.
         *
         * Override this method in order to change the formatted output.
         *
         * @param {string} value    The value to format.
         * @param {string} pattern  The format option.
         * @returns {*}
         */
        formatHelper: function(value, pattern) {
            const languageISO = _localizeService.currentLanguage;
            const separator = this.config[languageISO].CurrDecimalSep;
            const groupSep = this.config[languageISO].CurrGroupSep || "";
            const order = this.config[languageISO].CurrPositiveOrder; //0: Prefix, 1: Suffix, 2+3 are not supported and treated as 0
            const currencyExtension = this.bankingContext.currencyData.symbol || this.bankingContext.currencyData.iso;

            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> FormatService::formatHelper(value=${value}, pattern='${pattern}), current languageISO=${languageISO}`);

            let output = value;
            let expAbs = Math.abs(this.bankingContext.currencyData.exponent); //Exponent

            if(pattern === "#C" && jQuery.isNumeric(value)) { // 10.00 or 1,000.00
                output = parseInt(value);
                output /= Math.pow(10, expAbs);
                output = parseFloat(output).toFixed(expAbs);
                output = output.toString();
                output = output.replace(".", separator);
                output = addGroupSeparator(output, separator, groupSep);
            } else if(pattern === "#M" && jQuery.isNumeric(value)) { // €10.00 or €1,000.00
                output = parseInt(value);
                output /= Math.pow(10, expAbs);
                output = parseFloat(output).toFixed(expAbs);
                output = output.toString();
                output = output.replace(".", separator);
                output = addGroupSeparator(output, separator, groupSep);
                if (order === 1) {
                    output += ` ${currencyExtension}`;
                } else { //0 and others (2+3)
                    output = currencyExtension + " " +  output;
                }
            } else if(pattern === "#ATRM0" && jQuery.isNumeric(value)) { // €0 or €10 or or €1,000 (with group separator) or €0.10 for a 10 cent coin instead of €0.1
                output = parseInt(value);
                let fixIt = output && output < 100; // determine if its less than 1 EUR, but > 0
                output /= Math.pow(10, expAbs);
                if(fixIt) {
                    output = parseFloat(output).toFixed(expAbs); // we want 0.10 for a 10 cent coin instead of 0.1
                }
                output = output.toString();
                output = output.replace(".", separator);
                output = addGroupSeparator(output, separator, groupSep);
                if (order === 1) {
                    output += " " + currencyExtension;
                } else { //0 and others (2+3)
                    output = `${currencyExtension} ${output}`;
                }
            } else if(pattern ==="#SSN" && jQuery.isNumeric(value)) { // 123-45-6789
                let outputSSNString = output.toString();
                output = output.toString();
                let len = outputSSNString.length;
                switch (len) {
                    case 1:
                    case 2:
                    case 3:
                        break;
                    case 4:
                    case 5: outputSSNString = `${output.substr(0, 3)}-${output.substr(3, len - 3)}`;
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        outputSSNString = `${output.substr(0, 3)}-${output.substr(3, 2)}-${output.substr(5, len - 5)}`;
                        break;
                    default:
                        break;
                }
                output = outputSSNString;
            } else if(pattern.indexOf("#x+-") === 0) { // #x+-4:4 xxxxxxx1234
                let len = value.length;
                let res = "";
                let remainingLen = parseInt(pattern.substring(pattern.indexOf(":") + 1));
                for(let i = 0; i < len - remainingLen; i++) {
                    res += "x";
                }
                output = res + value.substring(len - remainingLen, len);
            } else if(pattern.indexOf("#X") === 0) { // **********
                output = output.toString().replace(/./g, pattern.substr(2)); // the part after the '#X' is the char to replace with
            } else if(pattern === "#i" && jQuery.isNumeric(value)) { // 10
                output = value.substr(0, value.length - expAbs);
            } else if(pattern.indexOf("#conditional") === 0) {
                try {
                    let replacementObject = JSON.parse(pattern.replace("#conditional", ""));
                    if(value in replacementObject) {
                        output = replacementObject[value];
                    } else if("*" in replacementObject){
                        output = replacementObject["*"];
                    } else {
                        output = value;
                    }
                } catch(e) {
                    _logger.error(`FormatService: Error formatting value=${value} with pattern=${pattern} exception: ${e.message}`);
                    output = value;
                }
                output = output.replace("$val", value);
            } else {
                if(value !== "" && pattern) { // in case of empty string we skip the log entry, since this could be the normal case in some circumstances
                    if (!jQuery.isNumeric(value)) {
                        _logger.error(`Can't format value='${value}' with pattern '${pattern}', because value is not a number`);
                    } else
                    {
                        _logger.error(`Can't format value='${value}' with unknown pattern '${pattern}'`);
                    }
                }
                output = value; // return original value
            }

            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `< FormatService::formatHelper return '${output}'`);
            return output;
        },
        

        /**
         * Reads the configuration for formatting from the Registry section "Activex\\GraphicalService\\PCHtmlGen\\(language)" and stores it in {@link Wincor.UI.Service.FormatService#config}
         * @param {object} languageMapping the language mapping from the FormatService
         * @returns {Promise}
         */
        readConfiguration: function(languageMapping) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> FormatService::readConfiguration(${JSON.stringify(languageMapping)})`);

            const self = this;
            const configPromises = [];

            function confCallback(lang, formatConf) {
                _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `. FormatService::readConfiguration confCallback(${lang}) - ${JSON.stringify(formatConf)}`);
                for (const key in formatConf) {
                    if (formatConf.hasOwnProperty(key) && (key.startsWith("Curr") || (key.startsWith("Num")))) { //only use keys starting with "Curr" and "Num", because all other keys are not relevant for formatting
                        const langIso = languageMapping.nameToIso[lang] ;
                        if (!self.config[langIso]) {
                            self.config[langIso] = {}; //the first time we come here, "self.config.de-DE" or self.config.en-US" will not exist
                        }
                        self.config[languageMapping.nameToIso[lang]] [key] = formatConf[key];
                        //_logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, ". FormatService::readConfiguration callback(" + langIso + ") - " + formatConf + ": " + JSON.stringify(self.config));
                    }
                }
            }

            for (const lang in languageMapping.nameToIso) { //GERMAN, ENGLISH
                _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `. FormatService::readConfiguration for(${lang})`);
                if (languageMapping.nameToIso.hasOwnProperty(lang)) {
                    configPromises.push(_configService.getConfiguration(`Activex\\GraphicalService\\PCHtmlGen\\${lang}`, null).then(confCallback.bind(null, lang)));
                }
            }

            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< FormatService::readConfiguration()");
            return ext.Promises.Promise.all(configPromises);
        },

        // begin TOOLING speciality - this should be removed
        /**
         * Reads the configuration for formatting from the Registry section "Activex\\GraphicalService\\PCHtmlGen\\(language)" and stores it in "this.config"
         * @param {object} languageMapping the language mapping from the FormatService
         * @returns {Promise}
         */
        readConfigurationTooling: function(languageMapping) {
            _logger.log(_logger.LOG_SRVC_INOUT, `> FormatService::readConfigurationTooling(${JSON.stringify(languageMapping)})`);
            const self = this;
            const LANGUAGE_MAPPINGS_FILE = "../servicemocks/mockdata/LanguageMappings.json";

            return ext.Promises.promise((resolve, reject)=> {
                ext.Promises.Promise.all([self.retrieveJSONData(LANGUAGE_MAPPINGS_FILE)]).then(dataArray => {
                    let isoCode;
                    const langSpecs = dataArray[0]["LanguageSpecifications"];
                    for(let i = 0; i < langSpecs.length; i++) {
                        isoCode = Object.keys(langSpecs[i])[0]; // get e.g. "en-US"
                        self.config[isoCode] = langSpecs[i][isoCode];
                    }
                    _logger.log(_logger.LOG_SRVC_INOUT, "< FormatService::readConfigurationTooling()");
                    resolve();
                }).catch(e => {
                    _logger.error("* importReference error getting " + LANGUAGE_MAPPINGS_FILE );
                    reject(e);
                });
            });
        },
        // end TOOLING speciality - this should be removed

        /**
         * See {@link Wincor.UI.Service.BaseService#onServicesReady}.
         *
         * Stores references to the LocalizeSerivce and the ConfigService.
         * As soon as these services are ready, it stores the configuration,
         * which is retrived from the LocalizeService, see {@link Wincor.UI.Service.LocalizeService#getLanguageMapping}.
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @eventhandler native
         * @lifecycle service
         */
        onServicesReady: function($super) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> FormatService::onServicesReady()");

            const self = this;

            _localizeService = self.serviceProvider.LocalizeService;
            _configService = self.serviceProvider.ConfigService;
            const servicesReady = [_localizeService.whenReady, _configService.whenReady]; //LocalizeService must be ready, because we need the mapping. ConfigService is needed when we call readConfiguration

            return ext.Promises.Promise.all(servicesReady).then(function () {
                // begin TOOLING specific - remove when tooling is handling PCHTMLGen data
                const viewService = self.serviceProvider.ViewService;
                // using 'hasOwnProperty' to avoid errorlog from service proxy if attribute is not available...
                if(!viewService.hasOwnProperty("TOOLING_MODE") || !viewService.TOOLING_MODE) {
                    return self.readConfiguration(_localizeService.getLanguageMapping()).then(result => {
                        _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `* FormatService:  ${JSON.stringify(self.config)}, result: ${JSON.stringify(result)}`);
                        _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< FormatService::onServicesReady");
                        return $super();
                    });
                } // end TOOLING speciality - this should be removed
                else { // begin TOOLING speciality - this should be removed
                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, "FormatService::onServicesReady call readConfigurationTooling");
                    return self.readConfigurationTooling(_localizeService.getLanguageMapping()).then(result => {
                        _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `* FormatService:  ${JSON.stringify(self.config)}, result: ${JSON.stringify(result)}`);
                        _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< FormatService::onServicesReady");
                        return $super();
                    });
                }
                // end TOOLING speciality - this should be removed
            });
        },

        /**
         * See {@link Wincor.UI.Service.PTService#initialize}.
         *
         * Inititializes the FormatService-specific FRM_RESOLVE_REQUEST parameters, so that the CCFormat module is used.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function ($super) {
            $super(); // Invoke superclass's initialize
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> FormatService::initialize()");

            this.FRM_RESOLVE_REQUEST.service = this.NAME;
            this.FRM_RESOLVE_REQUEST.FWName = "CCFormat";
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< FormatService::initialize");
        }
    });
    return Wincor.UI.Service.FormatService;
});