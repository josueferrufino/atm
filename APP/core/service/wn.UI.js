/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
console.log("__wn.UI__"),define([],function(){console.log("AMD:wn.UI");let n;return n=window.Wincor||{},n.UI=n.UI||{},n.UI.Service=n.UI.Service||{},n.isArray=function(n){return Array.isArray(n)},n.check4ValidObject=function(n){return void 0!==n&&null!==n},n.ConvHexToStr=function(n){return function(n){let r=null;return function(n){return n.length%2==0}(n)&&function(n){for(let r=0;r<n.length;r++){let e=n[r];if(!(e>="A"&&e<="F"||e>="a"&&e<="f"||e>="0"&&e<="9"))return!1}return!0}(n)&&(r=function(n){let r="";for(let e=0;e<n.length;e++)e===n.length-1&&0===n[e]||(r+=String.fromCharCode(n[e]));return r}(function(n){let r=[];for(let e=0;e<n.length;e+=2){let t=`0x${n[e]}${n[e+1]}`;r.push(parseInt(t))}return r}(n))),r}(n)},window.Wincor=n,n});