/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
console.log("__wn.UI.Gateway.Qt__"),define(["service/wn.UI.Gateway"],function(e){"use strict";return console.log("AMD:wn.UI.Gateway.Qt"),Wincor.UI.Gateway.Qt=Class.create(Wincor.UI.Gateway,{establishConnection:function($super){var e=window.PTBridge;if(void 0!==e){this.setBridge(e);var n=this.onMessage.bind(this);e.onMessage.connect(function(e){n(e)}),this.onOpen(null)}else Wincor.UI.Diagnostics.LogProvider.error("Can't establish a connection due to bridge is undefined !")},destroyConnection:function(e){null!==this.getBridge()&&(Wincor.UI.Gateway.GatewayProvider.setGateway(null),e&&e())},initialize:function($super){this.setName("Qt")}}),Wincor.UI.Gateway.Qt});