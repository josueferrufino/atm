/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
console.log("__wn.UI.Gateway.WebSocket__"),define(["service/wn.UI.Gateway"],function(t){"use strict";console.log("AMD:wn.UI.Gateway.WebSocket");let e=[];Wincor.UI.Gateway.Websocket=Class.create(Wincor.UI.Gateway,{socketPort:8091,hsID:0,establishConnection:function($super){var t=new WebSocket("ws://127.0.0.1:"+this.socketPort);this.setBridge(t),t.onopen=this.onOpen.bind(this),t.onclose=this.onClose.bind(this),t.onerror=this.onError.bind(this),t.onmessage=this.onMessage.bind(this)},destroyConnection:function(t){var e=this.getBridge();null!==e&&e.close?(e.onclose=function(){this.onClose("destroyConnection (WebSocket) invoked"),Wincor.UI.Gateway.GatewayProvider.setGateway(null),t&&t()}.bind(this),this.isSocketOk()?e.close():Wincor.UI.Gateway.GatewayProvider.setGateway(null)):Wincor.UI.Gateway.GatewayProvider.setGateway(null)},onOpen:function($super,t){if($super(t),0!==this.hsID){var n={};n.hsID=this.hsID;var s=this.posttranslateOutgoingMessage(n);this.getBridge().send(s),this.hsID=0}e.length>0&&(e.forEach(function(t,e,n){var s=this.posttranslateOutgoingMessage(t);this.getBridge().send(s)}),e=[])},isSocketOk:function(){var t=this.getBridge();return t&&t.readyState===t.OPEN},checkConnection:function($super){var t=this.isSocketOk();return t||this.establishConnection($super),t},pretranslateIncomingMessage:function($super,t){return JSON.parse(t.data)},posttranslateOutgoingMessage:function($super,t){return JSON.stringify(t)},send:function($super,t){this.checkConnection()?$super(t):e.push(t)},initialize:function($super){this.setName("WEBSOCKET")}})});