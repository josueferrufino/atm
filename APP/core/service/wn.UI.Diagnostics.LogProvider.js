/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["wn-ui","diagnostics","service/wn.UI.Diagnostics.ConsoleLogger"],function(e,g,r){console.log("AMD:wn.UI.Diagnostics.LogProvider");const o={20:"LOG_ANALYSE",21:"LOG_EXCEPTION",22:"LOG_ERROR",23:"LOG_WARNING",24:"LOG_INFO",25:"LOG_INOUT",26:"LOG_DATA",27:"LOG_SRVC_INOUT",28:"LOG_SRVC_DATA",29:"LOG_DETAIL"};return Wincor.UI.Diagnostics.LogProvider=Class.create({LOG_ANALYSE:20,LOG_EXCEPTION:21,LOG_ERROR:22,LOG_WARNING:23,LOG_INFO:24,LOG_INOUT:25,LOG_DATA:26,LOG_SRVC_INOUT:27,LOG_SRVC_DATA:28,LOG_DETAIL:29,logger:null,loggerHasTraceBits:!0,LOGGER_TYPE_CONSOLE:"CONSOLE",LOGGER_TYPE_TRACELOG:"TRACELOG",NAME:"LogProvider",log:function(e,g){try{this.loggerHasTraceBits?this.logger.log(e,g):this.logger.log(g)}catch(e){r.log(g)}},error:function(e){try{this.logger.error(e)}catch(g){r.log("*** ERROR *** "+e)}},onTraceBitsChanged:function(e){Object.keys(e).forEach(g=>this[o[g]]=!!e[g]&&parseInt(g,10))},setLogger:function(e){this.logger&&this.logger.detachLogger&&this.logger.detachLogger(),this.logger=e,this.logger.attachLogger&&this.logger.attachLogger(),this.loggerHasTraceBits=void 0!==e.isTraceBitSet,this.loggerHasTraceBits&&e.registerTraceBitsChangedHandler&&(e.registerTraceBitsChangedHandler(this.onTraceBitsChanged.bind(this)),this.logger.readCurrentTraceBitStates()),console.log(`. setLogger - loggerHasTraceBits=${this.loggerHasTraceBits}`)},initialize:function(){this.setLogger(r)}}),Wincor.UI.Diagnostics.LogProvider=new Wincor.UI.Diagnostics.LogProvider,Wincor.UI.Diagnostics.LogProvider});