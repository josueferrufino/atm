/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

/**
 * The pleasewait code-behind provides the life-cycle for the <i>pleasewait</i> view (touch only).
 * @module transactionprogress
 * @since 1.0/00
 */
define(['code-behind/baseaggregate', 'vm/MessageViewModel'], function(base) {
    console.log("AMD:transactionprogress");

    const _logger = Wincor.UI.Service.Provider.LogProvider;

    return /** @alias module:transactionprogress */ base.extend({
        name: "transactionprogress",

        /**
         * Instantiates the {@link Wincor.UI.Content.MessageViewModel} viewmodel.
         * @see {@link module:baseaggregate.activate}.
         * @lifecycle view
         * @return {Promise} resolved when activation is ready.
         */
        activate: function() {
            _logger.log(_logger.LOG_DETAIL, "* | VIEW_AGGREGATE transactionprogress:activate");
            base.container.add(new Wincor.UI.Content.MessageViewModel(), "flexMain");
            return base.activate();
        },

        /**
         * Overridden.
         * @see {@link module:baseaggregate.deactivate}.
         * @lifecycle view
         */
        compositionComplete: function(view, parent) {
            _logger.log(_logger.LOG_DETAIL, "* | VIEW_AGGREGATE transactionprogress:compositionComplete");
            base.compositionComplete(view, parent);
            base.container.whenActivated().then(function() {
                // code your specific stuff here
            });
        }
    });
});