/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["pt-service", "extensions"], function(ptsvc, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.ConfigService");

    /**
     *
     * @type {Wincor.UI.Diagnostics.LogProvider|*|Wincor.UI.Diagnostics.LogProvider|*}
     * @const
     * @private
     */
    const _logger = Wincor.UI.Diagnostics.LogProvider;

    /**
     * The class ConfigService supports reading configuration parameters from the Registry (via the DataFW).
     * @class
     * @since 1.0/10
     */
    Wincor.UI.Service.ConfigService = Class.create(Wincor.UI.Service.PTService/**@lends Wincor.UI.Service.ConfigService.prototype*/, {

        /**
         * "ConfigService" - the logical name of this service as used in the service-provider
         * @const
         * @type {string}
         */
        NAME: "ConfigService",

        /**
         * Holds the configuration for the ConfigService, currently only the instance name.
         * @property {Object} configuration
         * @property {String} configuration.instanceName the name of the instance e.g. <i>'GUIAPP', 'GUIDM' or 'GUIVIDEO'</i>
         */
        configuration: { instanceName: "" },

        /**
         * @deprecated
         */
        lastSection: "",

        /**
         * @see {@link Wincor.UI.Service.BaseService#onSetup}
         *
         * Initially, the ConfigService needs the instance name (GUIAPP, GUIDM, ...), because this is needed for further requests.
         *
         * @param {object} message      A configuration object, which constains the instanceName.
         * @lifecycle service
         */
        onSetup: function(message) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> ConfigService::onSetup('${JSON.stringify(message)}')`);
            this.configuration.instanceName = message.instanceName;
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< ConfigService::onSetup");
            return ext.Promises.Promise.resolve(); // synchronous fulfillment
        },

        /**
         * Gets a configuration from the central config source, e.g. Windows registry.
         *
         * TODO add some examples here, because the DataFW restrictions are a bit hard to explain. This examples should also show the result structure.
         *
         * @param {string} section                  Name e.g. "GUIAPP\\Services\\Timeouts"
         * @param {Array<string> | string} keys     E.g. ["DEFAULT_PAGE_TIMEOUT", "DEFAULT_INPUT_TIMEOUT", ...] or null for requesting the whole section
         * @param {function=} callback              Callback function, which receives the result. See {@link Wincor.UI.Service.ConfigService#translateResponse} for the result structure.
         */
        getConfiguration: function(section, keys, callback) {

            this.lastSection = section;
            keys = Array.isArray(keys) ? keys : [keys];

            let path;
            if (section.startsWith(this.configuration.instanceName)) { //it starts with GUIAPP or GUIDM
                if (keys[0] === null) {
                    path = `CCOPEN\\GUI\\${section}`;
                } else {
                    path = `.\\CCOPEN\\GUI\\${section}`;
                }
            } else { // it starts with some other section
                if (keys[0] === null) { // a whole section is requested ?
                    path = section; // we assume it's something located under ProTopas/CurrentVersion directly here - whole section reading works only under "\ProTopas\"
                } else if(!section.startsWith("\\")) {
                    path = `.\\${section}`;
                } else { // We have a single parameter key name here.
                         // Because the DataFW has the limitation that requesting a section outside of "\ProTopas" only works for single parameter not for the whole section
                    path = section;
                }
            }

            this.FRM_RESOLVE_REQUEST.service = this.NAME;
            this.FRM_RESOLVE_REQUEST.FWName = "CCDataFW";

            this.FRM_RESOLVE_REQUEST.FWFuncID = 7;  //CCDATAFW_FUNC_GET_STRING
            this.FRM_RESOLVE_REQUEST.param1 = path;
            if (keys[0] !== null) {
                this.FRM_RESOLVE_REQUEST.param2 = keys;
            } else {
                this.FRM_RESOLVE_REQUEST.param2 = [];
            }
            this.FRM_RESOLVE_REQUEST.param3 = [];

            _logger.LOG_ANALYSE && _logger.log(_logger.LOG_ANALYSE, `ConfigService::getConfiguration() Request to send: '${JSON.stringify(this.FRM_RESOLVE_REQUEST)}'.`);
            this.FrmResolve(this.FRM_RESOLVE_REQUEST, callback);
        },

        /**
         * Called automatically as soon as there is an answer to an asynchronous ProTopas request. See {@link Wincor.UI.Service.BaseService#translateResponse}.
         * The returend object depends wether a single key or a complete section was requested.
         *
         * @param {object} message    Response object, see {@link Wincor.UI.Service.BaseService#translateResponse}.
         * @returns {object}        A JSON object:
         *                          Each object parameter will be one of the requested keys/section.
         *                          Each value will be either a string (if exactly one parameter was requested) or a JSON object (if a complete section was requested).
         */
        translateResponse: function (message) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> ConfigService::translateResponse('${JSON.stringify(message)}')`);
            const response = {};
            if (message.FWFuncID === 7 && message.RC === 0) {
                //
                //  message.param2 has the keys,   i.e. [key1,   key2,   ...]
                //  message.param3 has the values, i.e. [value1, value2, ...]

                //  response will be an object:
                //  response["key1"] = value1
                //  response["key2"] = value2
                //  ...

                for (let i in message.param2) {
                    if (message.param2.hasOwnProperty(i)) {
                        let myKey = message.param2[i].toString();

                        try {
                            // try to copy a JSON object
                            response[myKey] = JSON.parse(message.param3[i]);
                        }
                        catch (e) {
                            // copy the string value
                            response[myKey] = message.param3[i];
                        }
                    }
                }
            } else {
                _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `Wincor.UI.Service.ConfigService(onResponse): message with FWFuncID '${message.FWFuncID}' and RC = '${message.RC}' will not be evaluated.`);
            }
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `< ConfigService::translateResponse returns ${JSON.stringify(response)}`);
            return response;
        },

        /**
         * @see {@link Wincor.UI.Service.PTService#initialize}.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super(); // Invoke superclass's initialize
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> ConfigService::initialize()");
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< ConfigService::initialize");
        }

    });
    return Wincor.UI.Service.ConfigService;
});