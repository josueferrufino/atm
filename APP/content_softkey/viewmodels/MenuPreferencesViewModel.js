/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
define(['knockout', 'extensions', 'vm-util/UIMovements', 'vm/ListViewModel'], function(ko, ext, objectManager) {
    "use strict";
    console.log("AMD:MenuPreferencesViewModel");

    const _logger = Wincor.UI.Service.Provider.LogProvider;
    const _dataService = Wincor.UI.Service.Provider.DataService;

    const KEY_SESSION_STORAGE_PRIORITIES = "menuPriorities";

    /**
     * Property key for access to the current menu preference of the customer.
     * @const
     * @type {string}
     */
    const PROP_MENU_PREFERENCE = _dataService.getKey("PROP_MENU_PREFERENCE");

    /**
     * This class is used for configuring the customers menu preferences.
     * @class
     */
    Wincor.UI.Content.MenuPreferencesViewModel = Class.create(Wincor.UI.Content.ListViewModel/**@lends Wincor.UI.Content.MenuPreferencesViewModel.prototype*/, {

        /**
         * Checks if command CONFIRM is available, if so it delegates execution to {@link Wincor.UI.Content.MenuPreferencesViewModel#onButtonPressed}
         * @param $super {Wincor.UI.Content.ListViewModel#observe} The corresponding function of the base class.
         * @param observableAreaId {String} The HTML element id to bind this viewmodel against.
         * @param viewName {String} The current views name for passing to ListViewModel instance
         */
        observe: function($super, observableAreaId, viewName) {
            _logger.LOG_ANALYSE && _logger.log(_logger.LOG_ANALYSE, "> MenuPreferencesViewModel::observe(" + observableAreaId + ")");
            $super(observableAreaId, viewName);
            this.cmdRepos.whenAvailable([this.STANDARD_BUTTONS.CONFIRM]).then(() => this.cmdRepos.addDelegate({id: this.STANDARD_BUTTONS.CONFIRM, delegate: this.onButtonPressed, context: this}));
            _logger.log(_logger.LOG_INOUT, "< MenuPreferencesViewModel::observe");
        },

        /**
         * If CONFIRM is pressed, the current order of items is written to the <a href="./BusinessPropertyKeyMap.json">PROP_MENU_PREFERENCE</a> property of the business logic.
         * @param $super {Wincor.UI.Content.ListViewModel#onButtonPressed} The corresponding base class function reference
         * @param id {String} ID of the command that triggered this action
         * @return {boolean} As a delegate returning true to avoid standard action
         */
        onButtonPressed: function($super, id) {
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "> MenuPreferencesViewModel::onButtonPressed(" + id + ")");
            let priorities;
            if(!this.designMode) {
                if(id === this.STANDARD_BUTTONS.CONFIRM) {
                    priorities = JSON.stringify(objectManager.getElementOrder());
                    _logger.LOG_DATA && _logger.log(_logger.LOG_DATA, ". MenuPreferencesViewModel::onButtonPressed priorities=" + priorities);
                    if(!this.designMode) {
                        this.serviceProvider.DataService.setValues(PROP_MENU_PREFERENCE, priorities, () => $super(id));
                    }
                    else { // design mode
                        window.sessionStorage.setItem(KEY_SESSION_STORAGE_PRIORITIES, priorities);
                        $super(id);
                    }
                }
                else {
                    $super(id);
                }
            }
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "< MenuPreferencesViewModel::onButtonPressed");
            return true;
        }
    });
});

