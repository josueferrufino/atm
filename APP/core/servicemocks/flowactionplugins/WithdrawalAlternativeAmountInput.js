/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

/**
 * @module flowactionplugins/WithdrawalAlternativeAmountInput
 */
define(["extensions", "../flowactionplugins/cashHelper"], function(ext, cashHelper) {
    "use strict";
    console.log("AMD:WithdrawalAlternativeAmountInput");

    const _dataService = Wincor.UI.Service.Provider.DataService;
    const _controlPanelService = Wincor.UI.Service.Provider.ControlPanelService;
    const PROP_FUNC_SEL_CODE = _dataService.getKey("PROP_INTERNAL_FUNCTION_SELECTION");
    const PROP_UNFORMATTED_VALUE = _dataService.getKey("PROP_UNFORMATTED_VALUE");
    const PROP_NOTE_DISPENSE_AMOUNT = _dataService.getKey("PROP_NOTE_DISPENSE_AMOUNT");
    const PROP_COIN_DISPENSE_AMOUNT = _dataService.getKey("PROP_COIN_DISPENSE_AMOUNT");

    return {

        /**
         * Emulates a business logic flow action: Sets the mixture display data property.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // the current view key (corresponding this this plug-in name)</li>
         * <li>currentFlow     // the current flow</li>
         * <li>action          // the current action (customer selection)</li>
         * <li>config          // Configuration of Config.js</li>
         * <li>container       // ViewModelContainer</li>
         * <li>serviceProvider // a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved (may argument a new view key destination) when the action is ready or rejected on any error.
         */
        onGuiResult: function(context) {
            return ext.Promises.promise(async (resolve, reject) => {
                try {
                    if(context.action === "CONFIRM" || context.action === "MIXTURE") {
                        if(_controlPanelService.getContext().cashInfosViewModel) {
                            let result = await _dataService.getValues([PROP_UNFORMATTED_VALUE], null, null);
                            _controlPanelService.getContext().cashInfosViewModel.amount(result[PROP_UNFORMATTED_VALUE]);
                        }
                        await _dataService.setValues(["PROP_PENULTIMATE_VIEWKEY", "PROP_PENULTIMATE_ACTION"], ["WithdrawalAlternativeAmountInput", context.action === "MIXTURE" ? "MIXTURE" : ""], null);
                        // check&set for alternative
                        await cashHelper.createCashMixture(context);
                    }
                    resolve();
                } catch(e) {
                    reject("FlowAction plug-in WithdrawalAlternativeAmountInput has been failed " + e);
                }
            });
        }
    };
});
