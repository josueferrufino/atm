/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

/**
 * @module flowactionplugins/PinEntry
 */
define(["jquery", "extensions"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:PinEntry");

    const _dataService = Wincor.UI.Service.Provider.DataService;
    const _eppService = Wincor.UI.Service.Provider.EppService;
    const _eventService = Wincor.UI.Service.Provider.EventService;
    const _controlPanelService = Wincor.UI.Service.Provider.ControlPanelService;

    const PROP_ETS_LAYOUT = _dataService.getKey("PROP_ETS_LAYOUT");
    const PROP_PIN_OPTIONS = _dataService.getKey("PROP_PIN_OPTIONS"); //a comma separated list: MIN,MAX,AUTOCONFIRM
    const PROP_PIN_ENTERED = "CCTAFW_PROP_VALID_PIN_ENTERED";
    const EVENT_INFO = _eventService.getEventInfo("EPP_MODULE");
    
    // simulation time for ready event (ENTER_DATA)
    const EPP_READY_SIM_TIME = 500;
    
    // simulation time for ETS ETS_LAYOUT_MOVED
    const ETS_READY_SIM_TIME = 1000;
    
    
    function pad(n, width, z) {
        z = z || '0';
        n += '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    return {

        /**
         * Emulates a business logic flow action when running: Sends an event for EPP ETS layout, if corresponding property 'PROP_ETS_LAYOUT' is set.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // the current view key (corresponding this this plug-in name)</li>
         * <li>config          // Configuration of Config.js</li>
         * <li>container       // ViewModelContainer</li>
         * <li>serviceProvider // a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved (may argument a new view key destination) when the action is ready or rejected on any error.
         */
        onActivateReady: async function(context) {
            try {
                let result = await _dataService.getValues([PROP_ETS_LAYOUT, PROP_PIN_OPTIONS]);
                let isEtsMode = result[PROP_ETS_LAYOUT] && result[PROP_ETS_LAYOUT] !== "";
                if(isEtsMode) {
                    const $etsContainer = jQuery("#flexEtsContainerArea");
                    if($etsContainer.length) {
                        const pos = $etsContainer.position();
                        let randomX = Math.round(Math.random() * (window.innerWidth - pos.left - $etsContainer.width()) + 1);
                        let randomY = Math.round(Math.random() * (window.innerHeight - pos.top - $etsContainer.height() / 2) + 1);
                        let wordX = randomX.toString(16), wordY = randomY.toString(16); // convert to hex values
                        // create an offset value and convert to negative hex values
                        if(randomX > pos.left) {
                            wordX = (-randomX >>> 0).toString(16);
                            if(wordX.length > 4) {
                                wordX = wordX.substr(wordX.length - 4, 4);
                            }
                        }
                        if(randomY < pos.top) {
                            wordY = (-randomY >>> 0).toString(16);
                            if(wordY.length > 4) {
                                wordY = wordY.substr(wordY.length - 4, 4);
                            }
                        }
                        // pad with leading zeros
                        wordX = pad(wordX, 4);
                        wordY = pad(wordY, 4);
                        // change lowbyte/highbyte order
                        wordX = wordX.substr(2, 2) + wordX.substr(0, 2);
                        wordY = wordY.substr(2, 2) + wordY.substr(0, 2);
                        setTimeout(() => {
                            _eventService.onEvent({
                                FWName: EVENT_INFO.NAME,
                                FWEventID: EVENT_INFO.ID_ETS_LAYOUT_MOVED,
                                FWEventParam: "02000000" + wordX + wordY
                            });
                            _eventService.onEvent({
                                FWName: EVENT_INFO.NAME,
                                FWEventID: EVENT_INFO.ID_ENTER_DATA
                            });
                        }, ETS_READY_SIM_TIME); // hardware sim time
                    }
                } else {
                    if(window.localStorage.getItem("controlPanelActive") !== "true") { // PIN entry simulation...
                        let value = result[PROP_PIN_OPTIONS];
                        if(value && value !== "") {
                            value = value.split(",");
                            try {
                                setTimeout(() => {
                                    _eventService.onEvent({
                                        FWName: EVENT_INFO.NAME,
                                        FWEventID: EVENT_INFO.ID_ENTER_DATA
                                    });
                                    let minPinLength = parseInt(value[0]); // MIN
                                    //let maxPinLength = parseInt(value[1]); // MAX
                                    //let isAutoConfirm = !!(value[2] === "1"); // AUTO CONFIRM
                                    for(let i = 0; i < minPinLength; i++) {
                                        window.setTimeout(() => _eppService.onEvent({methodName: "KeyPressed", key: "*"}), 400 * (i + 1));
                                    }
                                }, EPP_READY_SIM_TIME); // hardware sim time
                            } catch(e) {
                                throw `Pin options parsing error ! ${e.message}`; // OK to be caught by upper catch
                            }
                        }
                    } else {
                        setTimeout(() => {
                            _eventService.onEvent({
                                FWName: EVENT_INFO.NAME,
                                FWEventID: EVENT_INFO.ID_ENTER_DATA
                            });
                        }, EPP_READY_SIM_TIME); // hardware sim time
                    }
                }
            } catch(e) {
                throw `FlowAction plug-in PinEntry::onActivateReady has been failed ${e}`;
            }
        },

        /**
         * Emulates a business logic flow action: Sends an event for EPP ETS layout, if corresponding property 'PROP_ETS_LAYOUT' is set.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // the current view key (corresponding this this plug-in name)</li>
         * <li>currentFlow     // the current flow</li>
         * <li>action          // the current action (customer selection)</li>
         * <li>config          // Configuration of Config.js</li>
         * <li>container       // ViewModelContainer</li>
         * <li>serviceProvider // a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved (may argument a new view key destination) when the action is ready or rejected on any error.
         */
        onGuiResult: function(context) {
            return ext.Promises.promise(async (resolve, reject) => {
                await _dataService.setValues([PROP_PIN_ENTERED, "PROP_PENULTIMATE_VIEWKEY"], ["1", "PinEntry"], null); // check, set even PIN wrong?
                if(context.action === "CONFIRM" && _controlPanelService.getContext().transactionViewModel && _controlPanelService.getContext().transactionViewModel.pinInvalid()) {
                    _controlPanelService.getContext().transactionViewModel.tryCounter(parseInt(_controlPanelService.getContext().transactionViewModel.tryCounter()) + 1);
                }
                resolve();
            });
        }
    };
});
