/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

define(["knockout", "vm/AnimationsViewModel"], function(ko) {
    "use strict";

    const _logger = Wincor.UI.Service.Provider.LogProvider;

    /**
     * Deriving from {@link Wincor.UI.Content.AnimationsViewModel} class. <BR>
     * This viewmodel handles the envelope animations.
     * @class
     */
    Wincor.UI.Content.DepositEnvelopeAnimationsViewModel = Class.create(Wincor.UI.Content.AnimationsViewModel /** @lends Wincor.UI.Content.DepositEnvelopeAnimationsViewModel.prototype */, {


        /**
         * This observable stores the label text for the envelope eject animation.
         * @type {ko.observable}
         * @bindable
         */
        animationTextTakeEnvelope: null,

        /**
         * This observable stores the label text for the envelope insert animation.
         * @type {ko.observable}
         * @bindable
         */
        animationTextInsertEnvelope: null,

        /**
         * This observable will be true, if the envelope eject animation is to be displayed.
         * @type {ko.observable}
         * @bindable
         */
        viewFlexEnvelopeEject: null,

        /**
         * This observable will be true, if the envelope insert animation is to be displayed.
         * @type {ko.observable}
         * @bindable
         */
        viewFlexEnvelopeInsert: null,

        /**
         * Initializes the DOM-associated objects.
         * Overrides {@link Wincor.UI.Content.AnimationsViewModel#observe}
         * @param {function=} $super            Reference to the corresponding function of the base class.
         * @param {string} observableAreaId     The area id to observe via knockout
         * @lifecycle viewmodel
         */
        observe : function ($super, observableAreaId) {
            _logger.LOG_ANALYSE && _logger.log(_logger.LOG_ANALYSE, "> DepositEnvelopeAnimationsViewModel::observe(" + observableAreaId + ")");
            $super(observableAreaId);
            if(this.designMode) {
                this.viewFlexEnvelopeEject(true);
                this.viewFlexEnvelopeInsert(true);
                this.animationTextTakeEnvelope("Envelope");
                this.animationTextInsertEnvelope("Envelope");
            }
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "< DepositEnvelopeAnimationsViewModel::observe");
        },

        /**
         * Sets the proper animation content depending on the given result array:
         * <ul>
         *     <li>TakeEnvelope</li>
         *     <li>InsertEnvelope</li>
         * </ul>
         * @param {function=} $super            Reference to the corresponding function of the base class.
         * @param {Array<String>} resultArray   The result content keys
         */
        setAnimations: function($super, resultArray) {
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "> DepositEnvelopeAnimationsViewModel::setAnimations(" + resultArray + ")");

            this.viewFlexEnvelopeEject(this.isAvailable(resultArray, "TakeEnvelope"));
            this.viewFlexEnvelopeInsert(this.isAvailable(resultArray, "InsertEnvelope"));

            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "< DepositEnvelopeAnimationsViewModel::setAnimations");
            $super(resultArray);
        },

        /**
         * Overrides {@link Wincor.UI.Content.AnimationsViewModel#onTextReady}
         * Depending on the value of each 'AnimationText_xyz' textkey, the animation observables are set.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @param {Object} result       Contains the key:value pairs of text previously retrieved by this view-model subclass.
         * @lifecycle viewmodel
         */
        onTextReady: function($super, result) {
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "> DepositEnvelopeAnimationsViewModel::onTextReady(...)");
            for(var key in result) {
                if(result.hasOwnProperty(key)) {
                    if(key.indexOf("AnimationText_TakeEnvelope") !== -1) {
                        this.animationTextTakeEnvelope(result[key]);
                    }
                    else if(key.indexOf("AnimationText_InsertEnvelope") !== -1) {
                        this.animationTextInsertEnvelope(result[key]);
                    }
                }
            }
            $super(result);
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "< DepositEnvelopeAnimationsViewModel::onTextReady");
        },

        /**
         * Overrides {@link Wincor.UI.Content.AnimationsViewModel#onDeactivated}
         * Is called when this viewmodel gets deactivated during the life-cycle.
         * @param {function} $super the base class function
         * @lifecycle viewmodel
         */
        onDeactivated: function($super) {
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "> DepositEnvelopeAnimationsViewModel::onDeactivated()");
            $super();
            this.viewFlexEnvelopeEject(false);
            this.viewFlexEnvelopeInsert(false);
            this.animationTextTakeEnvelope("");
            this.animationTextInsertEnvelope("");
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "< DepositEnvelopeAnimationsViewModel::onDeactivated()");
        },

        /**
         * Overrides {@link Wincor.UI.Content.AnimationsViewModel#initialize}
         * Initializes the members to become  observables.
         * @param {function} $super  reference of the super function of the base class
         * @lifecycle viewmodel
         */
        initialize: function($super) {
            $super();
            this.viewFlexEnvelopeEject = ko.observable(false);
            this.viewFlexEnvelopeInsert = ko.observable(false);
            this.animationTextTakeEnvelope = ko.observable("");
            this.animationTextInsertEnvelope = ko.observable("");
        }
    });
});
