/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.

*/
define(["jquery", "extensions", "servicemocks/wn.UI.Service.BaseServiceMock"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.FormatServiceMock");

    /**
     *
     * @type {Wincor.UI.Diagnostics.LogProvider|*|Wincor.UI.Diagnostics.LogProvider|*}
     * @private
     */
    const _logger = Wincor.UI.Diagnostics.LogProvider;
    var _localizeService;

    function addGroupSeparator(nStr, decSep, separator) {
        nStr += '';
        var x = nStr.split(decSep);
        var x1 = x[0];
        var x2 = x.length > 1 ? decSep + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while(rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + separator + '$2');
        }
        return x1 + x2;
    }

    /**
     * The FormatServiceMock provides standard format patterns for number formatting.
     * @class
     */
    Wincor.UI.Service.FormatServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock/** @lends Wincor.UI.Service.FormatServiceMock.prototype*/, {

        /**
         * "FormatServiceMock" - the logical name of this service as used in the service-provider
         * @const
         * @type {string}
         */
        NAME: "FormatService",
    
        /**
         * See {@link Wincor.UI.Service.FormatService#config}.
         */
        config: {},

        /**
         * See {@link Wincor.UI.Service.FormatService#bankingContext}.
         */
        bankingContext: {},
    
        /**
         * This method is called by the {@link Wincor.UI.Service.Provider#propagateError} if an error occurred in any service. It logs the error to the console.
         *
         *
         * @param {String} serviceName  The name of this service.
         * @param {String} errorType    As defined in {@link Wincor.UI.Service.BaseService#ERROR_TYPE}.
         */
        onError: function(serviceName, errorType) {
            _logger.log(_logger.LOG_SRVC_INOUT, "> FormatServiceMock::onError(" + serviceName + ", " + errorType + ")");
            _logger.log(_logger.LOG_SRVC_INOUT, "< FormatServiceMock::onError");
        },

        /**
         * See {@link Wincor.UI.Service.FormatService#format}.
         *
         * In contrast to the ProTopas FormatService, asynchronous requests, whose format options are not implemented in formatHelper(),
         * can not be supported by anyone else (for the ProTopas FormatService they _could_ possibly supported by the ProTopas FormatFW),
         * so there will be an error immediately.
         */
        format: function(value, formatOption, callback, isSynchronous) {
            _logger.log(_logger.LOG_SRVC_INOUT, "> FormatService::format(value=" + value + ", formatOption='" + formatOption + "', isSynchronous=" + isSynchronous + ")");
            var self = this;
            function generateSpecificResponse() {
                return self.formatHelper(typeof value === 'object' ? value.raw : value, formatOption);
            }
            if(!isSynchronous) {
                callback({ result: generateSpecificResponse.bind(this)(), raw: typeof value === 'object' ? value.raw : value });
            }
            else if(typeof value === "object") {
                _logger.log(_logger.LOG_SRVC_INOUT, "< FormatServiceMock::format");
                value.result = this.formatHelper(value.raw, formatOption);
            }
            else {
                _logger.error("Wrong format arguments. Please check arguments in order to handle format in a right way.");
            }
            _logger.log(_logger.LOG_SRVC_INOUT, "< FormatServiceMock::format");
        },

        /**
         * See {@link Wincor.UI.Service.FormatService#formatHelper}.
         */
        formatHelper: function(value, pattern) {
            const languageISO = _localizeService.currentLanguage;
            if(!this.config[languageISO]) {
                _logger.error(`Can't format value '${value}', because no language specific format configuration is available for ISO language culture name '${languageISO}'.`);
                return value;
            }
            const separator = this.config[languageISO].CurrDecimalSep;
            const groupSep = this.config[languageISO].CurrGroupSep || "";
            const order = this.config[languageISO].CurrPositiveOrder; //0: Prefix, 1: Suffix, 2+3 are not supported and treated as 0
            const currencyExtension = this.bankingContext.currencyData.symbol || this.bankingContext.currencyData.iso;

            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> FormatServiceMock::formatHelper(value=" + value + ", pattern='" + pattern + "), current languageISO=" + languageISO);

            let output = value;
            let expAbs = Math.abs(this.bankingContext.currencyData.exponent); //Exponent

            if(pattern === "#C" && jQuery.isNumeric(value)) { // 10.00 or 1,000.00
                output = parseInt(value);
                output /= Math.pow(10, expAbs);
                output = parseFloat(output).toFixed(expAbs);
                output = output.toString();
                output = output.replace(".", separator);
                output = addGroupSeparator(output, separator, groupSep);
            } else if(pattern === "#M" && jQuery.isNumeric(value)) { // €10.00 or €1,000.00
                output = parseInt(value);
                output /= Math.pow(10, expAbs);
                output = parseFloat(output).toFixed(expAbs);
                output = output.toString();
                output = output.replace(".", separator);
                output = addGroupSeparator(output, separator, groupSep);
                if(order === 1) {
                    output += " " + currencyExtension;
                } else { //0 and others (2+3)
                    output = currencyExtension + " " + output;
                }
            } else if(pattern === "#ATRM0" && jQuery.isNumeric(value)) { // €0 or €10 or or €1,000 (with group separator) or €0.10 for a 10 cent coin instead of €0.1
                output = parseInt(value);
                let fixIt = output && output < 100; // determine if its less than 1 EUR, but > 0
                output /= Math.pow(10, expAbs);
                if(fixIt) {
                    output = parseFloat(output).toFixed(expAbs); // we want 0.10 for a 10 cent coin instead of 0.1
                }
                output = output.toString();
                output = output.replace(".", separator);
                output = addGroupSeparator(output, separator, groupSep);
                if(order === 1) {
                    output += " " + currencyExtension;
                } else { //0 and others (2+3)
                    output = currencyExtension + " " + output;
                }
            } else if(pattern ==="#SSN" && jQuery.isNumeric(value)) { // 123-45-6789
                var outputSSNString = output.toString();
                output = output.toString();
                var len = outputSSNString.length;
                switch (len) {
                    case 1:
                    case 2:
                    case 3:
                        break;
                    case 4:
                    case 5: outputSSNString = output.substr(0, 3) + "-" + output.substr(3, len-3);
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        outputSSNString = output.substr(0, 3) + "-" + output.substr(3, 2)  + "-" + output.substr(5, len-5);
                        break;
                    default:
                        break;
                }
                output = outputSSNString;
            } else if(pattern.indexOf("#x+-") !== -1) { // #x+-4:4 xxxxxxx1234
                let len = value.length, res = "";
                let remainingLen = parseInt(pattern.substring(pattern.indexOf(":") + 1));
                for(let i = 0; i < len - remainingLen; i++) {
                    res += "x";
                }
                output = res + value.substring(len - remainingLen, len);
            } else if(pattern.indexOf("#X") !== -1) { // **********
                output = output.toString().replace(/./g, pattern.substr(2)); // the part after the '#X' is the char to replace with
            } else if(pattern === "#i" && jQuery.isNumeric(value)) { // 10
                output = value.substr(0, value.length - expAbs);
            } else if(pattern.indexOf("#conditional") === 0) {
                try {
                    let replacementObject = JSON.parse(pattern.replace("#conditional", ""));
                    if(value in replacementObject) {
                        output = replacementObject[value];
                    } else if("*" in replacementObject){
                        output = replacementObject["*"];
                    } else {
                        output = value;
                    }
                } catch(e) {
                    _logger.error("FormatService: Error formatting value=" + value + " with pattern=" + pattern + " exception: " + e.message);
                    output = value;
                }
                output = output.replace("$val", value);
            } else {
                if(value !== "" && pattern) { // in case of empty string we skip the log entry, since this could be the normal case in some circumstances
                    if (!jQuery.isNumeric(value)) {
                        _logger.error(`Can't format value='${value}' with pattern '${pattern}', because value is not a number`);
                    } else
                    {
                        _logger.error(`Can't format value='${value}' with unknown pattern '${pattern}'`);
                    }
                }
                output = value; // return original value
            }

            _logger.log(_logger.LOG_SRVC_INOUT, "< FormatServiceMock::formatHelper return '" + output + "'");
            return output;
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         *
         * @param {object} message      See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         * @returns {Promise}
         * @lifecycle service
         */
        onSetup: function(message) {
            _logger.log(_logger.LOG_SRVC_INOUT, "> FormatServiceMock::onSetup('" + JSON.stringify(message) + "')");
            return ext.Promises.promise(resolve => {
                resolve();
                _logger.log(_logger.LOG_SRVC_INOUT, "< FormatServiceMock::onSetup");
            });
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onServicesReady}
         
         * Stores references to the LocalizeSerivce.
         * As soon as this services is ready, it stores the configuration,
         * which is retrived from the LocalizeService, see {@link Wincor.UI.Service.LocalizeService#getLanguageSpecifications}.
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         */
        onServicesReady: function($super) {
            _logger.log(_logger.LOG_SRVC_INOUT, "> FormatServiceMock::onServicesReady()");
            _localizeService = this.serviceProvider.LocalizeService;
            const self = this;
            return _localizeService.whenReady.then(() => {
                self.config = _localizeService.getLanguageSpecifications();
                _logger.log(_logger.LOG_DETAIL, "* FormatServiceMock:  " + JSON.stringify(self.config));
                return $super();
            });
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#initialize}.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super();
            _logger.log(_logger.LOG_SRVC_INOUT, "> FormatServiceMock::initialize()");
            _logger.log(_logger.LOG_SRVC_INOUT, "< FormatServiceMock::initialize");
        }

    });

    return Wincor.UI.Service.FormatServiceMock;
});
