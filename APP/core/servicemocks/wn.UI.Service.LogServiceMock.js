/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
define(["servicemocks/wn.UI.Service.BaseServiceMock"], function() {
    "use strict";
    console.log("AMD:wn.UI.Service.LogServiceMock");

    /**
     * The LogServiceMock class provides methods for trace and error logging.
     * @class
     */
    Wincor.UI.Service.LogServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock /** @lends Wincor.UI.Service.LogServiceMock.prototype */, { //Hint: don't write a log in here!!!


        /**
         * "LogService" - the logical name of this service as used in the service-provider
         * @const
         * @type {string}
         */
        NAME: "LogService",

        /**
         * The type of this logger. Used by the service-provider.
         * @const
         * @type {string}
         */
        TYPE: "TRACELOG",

        /**
         * The trace level "trace"
         * @const
         * @type {string}
         */
        METHOD_LOG: "trace",

        /**
         * The trace level "error"
         * @const
         * @type {string}
         */
        METHOD_ERROR: "error",

        /**
         * The message that has to be logged
         * @type {string}
         */
        LOG_MESSAGE: null,

        /**
         * Set of trace bits
         * @type {string}
         */
        TRACEBIT_SET: null,

        /**
         * "traceBitStates" - the array containg the states for the trace bits
         * @type {object}
         */
        traceBitStates: {},

        traceBitsChangedHandler: [],


        /**
         * This method logs the given "data" with the traceBit as source identifier
         * The changed key will be updated in the DataRegistration structure, which contains all prior requested keys.
         * @param {int} traceBit
         * @param {string} data
         */
        log: function (traceBit, data) {
            // Simplify this to speed up things. Errors should be handled in native code
            if (traceBit === false) {
                return;
            }
            if (Wincor.UI.Gateway.GatewayProvider.getGateway()) {
                // TODO: Check if we really want to auto convert
                data = "" + data; // implicit conversion
                this.LOG_MESSAGE.traceBit = traceBit;
                this.LOG_MESSAGE.logText = data;
                this.sendEvent(this.LOG_MESSAGE);
            } else {
                console.log(data);
            }
        },

        /**
         * This method triggers an event, to log an error with the given data.
         * @param {string} data
         */
        error: function (data) {
            if (Wincor.UI.Gateway.GatewayProvider.getGateway()) {
                //TODO: Check if we really want to auto convert
                data = "" + data; // implicit conversion
                this.ERROR_MESSAGE.logText = data;
                this.sendEvent(this.ERROR_MESSAGE);
            } else {
              console.error(data);
            }
        },

        /**
         * Check if a trace bit is set.
         * @param {int} traceBit
         * @return {boolean}
         */
        isTraceBitSet: function (traceBit) {
            return this.traceBitStates[traceBit];
        },

        /**
         * Reads the tracebit states from business logic
         * @param callback Callback function is called when all responses did arrive
         */
        readCurrentTraceBitStates: function (callback) {
            if (callback) {
                callback();
            }
        },

        /**
         *
         * @param handler
         * @return {number}
         */
        registerTraceBitsChangedHandler: function(handler) {
            return -1;
        },

        removeTraceBitsChangedHandler: function(id) {
        },

        /**
         * Called from business logic to set tracebits, if they did change
         * @param {object} message
         */
        setTraceBits: function(message) {
        },

        /**
         *
         * @param {function} $super
         */
        onGatewayReady: function($super) {
            //TODO implemented, because it's also in normal LogService, but this function is never called (should be called in toolingEDM mode, because then we have a gateway)
            $super();
        },

        /**
         * See {@link Wincor.UI.Service.PTService#initialize}.
         * TODO: specialties
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function ($super) {
            this.traceBitStates = {
                20: true,
                21: true,
                22: true,
                23: true,
                24: true,
                25: true,
                26: true,
                27: true,
                28: true,
                29: true
            };
            $super();
            this.LOG_MESSAGE = Object.assign(Object.assign({}, this.EVENT), {
                service: this.NAME,
                eventName: this.METHOD_LOG,
                traceBit: null,
                logText: "" // text to log or write error
            });

            this.ERROR_MESSAGE = Object.assign(Object.assign({}, this.EVENT), {
                service: this.NAME,
                eventName: this.METHOD_ERROR,
                logText: "" // text to log or write error
            });

            this.TRACEBIT_SET = Object.assign(Object.assign({}, this.REQUEST), {
                service: this.NAME,
                methodName: "trcCheckBit",
                result: false
            });
        }
    });

    return Wincor.UI.Service.LogServiceMock;
});
