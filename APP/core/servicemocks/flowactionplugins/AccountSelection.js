/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
/**
 * @module flowactionplugins/AccountSelection
 */
define(["extensions"], function(ext) {
    "use strict";
    console.log("AMD:AccountSelection");

    const _dataService = Wincor.UI.Service.Provider.DataService;
    const PROP_FAC_OPTION = "CCTAFW_PROP_FAC_OPTION[1]";
    const PROP_CUSTOMER_INPUT_FROM_ACCOUNT = "CCTAFW_PROP_CUSTOMER_INPUT_VARIABLE[31]";
    const PROP_CUSTOMER_INPUT_TO_ACCOUNT = "CCTAFW_PROP_CUSTOMER_INPUT_VARIABLE[31]";
    
    return {
        
        /**
         * Emulates a business logic flow action: Switch the UI language to the desired one.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // the current view key (corresponding this this plug-in name)</li>
         * <li>currentFlow     // the current flow</li>
         * <li>action          // the current action (customer selection)</li>
         * <li>config          // Configuration of Config.js</li>
         * <li>container       // ViewModelContainer</li>
         * <li>serviceProvider // a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved (may argument a new view key destination) when the action is ready or rejected on any error.
         */
        onGuiResult: function(context) {
            return ext.Promises.promise((resolve, reject) => {
                try {
                    _dataService.getValues(PROP_FAC_OPTION, result => {
                        let fromAccountOption = result[PROP_FAC_OPTION];
                        let valueToSet = "2";
                        let customerInputVariableToSet = PROP_CUSTOMER_INPUT_FROM_ACCOUNT;

                        if (fromAccountOption === "2")
                        {
                            //after toAccount next is "unknown" to get out of the loop
                            valueToSet = "";
                            customerInputVariableToSet = PROP_CUSTOMER_INPUT_TO_ACCOUNT
                        }
                        
                        _dataService.setValues([PROP_FAC_OPTION,customerInputVariableToSet], [valueToSet,context.action], result => resolve());
                    });
                    
                }
                catch(e) {
                    reject("FlowAction plug-in AccountSelection has been failed " + e);
                }
            });
        }
    };
});
