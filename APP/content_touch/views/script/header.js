/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

/**
 * The header code-behind provides the life-cycle for the <i>header</i> view.
 * <p>
 * Please note that the header layout part is a static part which means that it's not gonna removed but updated when a new view come in.
 * </p>
 * @module header
 * @since 1.0/00
 */
define(['jquery', 'code-behind/baseaggregate', 'vm-util/UICommanding', 'vm/HeaderViewModel'], function($, base, commanding) {
    console.log("AMD:header");

    const _logger = Wincor.UI.Service.Provider.LogProvider;

    let _messageDisappearDeffered = null;

    const CMD_MAGNIFY_GLASS = "MAGNIFY_GLASS";


    return /** @alias module:header */ base.extend({
        name: "header",

        /**
         * Moves the header message out of the view.
         * @returns {Promise} gets resolved the movement is ready.
         */
        moveHeaderMessageOut: function() {
            if(base.config.VIEW_ANIMATIONS_ON) {
                if(_messageDisappearDeffered && _messageDisappearDeffered.promise.isPending()) {
                    _messageDisappearDeffered.reject();
                }
                _messageDisappearDeffered = Wincor.UI.deferred();
                    const msgContainer = $("#flexMessageContainerHeader");
                    const messageArea = msgContainer.find(".messageArea");
                    if(msgContainer.length && msgContainer.css("display") !== "none" && messageArea.length) {
                        messageArea.on("animationend webkitAnimationEnd", () => {
                            messageArea.css({animation: ""});
                            messageArea.off();
                            _messageDisappearDeffered.resolve();
                        });
                        messageArea.css({animation: "escalationMoveOut .5s ease-out"});
                    } else {
                        _messageDisappearDeffered.resolve();
                    }
                return _messageDisappearDeffered.promise.timeout(550);
            } else {
                return base.Promises.Promise.resolve();
            }
        },

        /**
         * Switches the magnify glass off.
         */
        magnifyGlassOff: function() {
            commanding.whenAvailable(CMD_MAGNIFY_GLASS).then(() => {
                // switch off 'eye' of magnify glass
                const $magnifyGlassIcon = $("#magnifyGlassObject");
                if($magnifyGlassIcon.length && $magnifyGlassIcon[0].getSVGDocument()) {
                    $magnifyGlassIcon[0].getSVGDocument().magnifyGlassOff();
                }
                commanding.setVisible(CMD_MAGNIFY_GLASS, false);
            });
        },

        /**
         * Instantiates the {@link Wincor.UI.Content.HeaderViewModel} viewmodel.
         * This function is only triggered once, at the very first composition of <i>shell.html</i>.
         * @see {@link module:baseaggregate.activate}.
         * @lifecycle view
         * @return {Promise} resolved when activation is ready.
         */
        activate: function() {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "* | VIEW_AGGREGATE header:activate");
            let headerVM = new Wincor.UI.Content.HeaderViewModel(this);
            headerVM.lifeCycleMode = base.container.LIFE_CYCLE_MODE.STATIC;
            base.container.add(headerVM, ["flexHeader"]);
            return base.activate();
        },

        /**
         * Overridden to set the <i>header</i> view fragment.
         * This function is only triggered once, at the very first composition of <i>shell.html</i>.
         * @param {HTMLElement} view the <i>header</i> view fragment
         * @returns {boolean | Promise}
         * @lifecycle view
         * @return {Promise} resolved when binding is ready.
         */
        binding: function(view) {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `* | VIEW_AGGREGATE header:binding viewId=${view.id}, moduleId=${base.system.getModuleId(this)}`);
            base.system.setHeader(view);
            return base.binding(view);
        },
    
        /**
         * Overridden to find the <code>data-dm-available</code> custom attribute and set it true
         * when marketing is available and a softkey based layout is active.
         * @see {@link module:baseaggregate.bindingComplete}.
         * @lifecycle view
         */
        bindingComplete: function(view, parent) {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `* | VIEW_AGGREGATE header:bindingComplete`);
            _logger.LOG_INFO && _logger.log(_logger.LOG_INFO, `* VIEW_AGGREGATE header:bindingComplete - isDirectMarketingAvailable = ${base.config.isDirectMarketingAvailable} SOFTKEY_LIMITATION_ON_ACTIVE_DM = ${base.config.SOFTKEY_LIMITATION_ON_ACTIVE_DM}` );
            if (base.config.isDirectMarketingAvailable && base.config.SOFTKEY_LIMITATION_ON_ACTIVE_DM ) {
                $("[data-view-key]").first().attr({"data-dm-available": true});
            }
            base.bindingComplete(view, parent);
        },

        /**
         * Overridden to set an effect at the instruction text in order when the view stays on next viewkey and the instruction text changes.
         * @see {@link module:baseaggregate.compositionComplete}.
         * @lifecycle view
         */
        compositionComplete: function(view) {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "* | VIEW_AGGREGATE header:compositionComplete");
            if(base.config.VIEW_ANIMATIONS_ON) {
                // react on view update and add animations to the instruction text line:
                base.router.on("router:navigation:composition-update").then(() => {
                    _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "* | VIEW_AGGREGATE header:compositionComplete->composition update");
                    const instr = $(view).find("#instruction");
                    instr.hide("slow", () => {
                        instr.show();
                        base.viewHelper.animate(instr, "zoomIn", "0.15s", "ease-out").then(() => {
                            base.viewHelper.resetAfterAnimate(instr);
                        });
                    });
                });
            }
            // on navigation to same or other view, handle headline & instruction
            let $headlineInstruction = $("#headline, #instruction");
            base.router.on("router:navigation:composition-update").then(() => {
                base.container.whenDeactivated().then(() => $headlineInstruction.addClass("disabledElement"));
                base.container.whenActivationStarted().then(() => $headlineInstruction.removeClass("disabledElement"));
            });
            base.router.on("router:navigation:complete").then(() => {
                base.container.whenDeactivated().then(() => $headlineInstruction.addClass("disabledElement"));
                base.container.whenActivationStarted().then(() => $headlineInstruction.removeClass("disabledElement"));
            });
        }
    });
});
