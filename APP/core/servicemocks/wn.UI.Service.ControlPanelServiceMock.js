/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["jquery", "extensions", "servicemocks/wn.UI.Service.BaseServiceMock"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.ControlPanelServiceMock");

    let _controlPanel = null;

    /**
     * The ControlPanelServiceMock handles services for the control panel.
     * @class
     * @since 2.0/10
     */
    Wincor.UI.Service.ControlPanelServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock/** @lends Wincor.UI.Service.ControlPanelServiceMock.prototype*/, {

        /**
         * The logical name of this service as used in the service-provider
         * @const
         * @type {string}
         * @default "ControlPanelService"
         */
        NAME: "ControlPanelService",

        /**
         * @see {@link Wincor.UI.Service.ViewService#SERVICE_EVENTS}.
         * @enum {string}
         */
        SERVICE_EVENTS: {
            /**
             * Fired when a local timer has been newed.
             */
            NEW_TIMEOUT: "NEW_TIMEOUT",
        },

        /**
         * Sets the control panel.
         * @param {Object} panel the panel window
         */
        setControlPanel: function(panel) {
            _controlPanel = panel;
        },

        /**
         * Gets the control panel.
         * @return {Object} the control panel window object
         */
        getControlPanel: function() {
            return _controlPanel;
        },

        /**
         * Gets the control panel context.
         * @return {Object} the control panel context object
         */
        getContext: function() {
            return (_controlPanel && _controlPanel.getContext && _controlPanel.getContext()) || {};
        },

        /**
         * Updates the business properties of the control panel.
         * @param {Map} propMap the property map contains a map of property keys and values
         */
        updateBusinessProperties: function(propMap) {
            if(_controlPanel && _controlPanel.businessPropertiesUpdate) {
                _controlPanel.businessPropertiesUpdate(propMap);
            }
        },

        /**
         * Should be called when a new local timer has been started.
         * @param {Number} timeLen the time length
         */
        newTimerStarted: function(timeLen) {
            this.fireServiceEvent(this.SERVICE_EVENTS.NEW_TIMEOUT, timeLen);
        },

        /**
         * @see {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         *
         * @param {Object} message      See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         * @returns {Promise}
         * @lifecycle service
         */
        onSetup: function(message) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, `> ControlPanelServiceMock::onSetup('${JSON.stringify(message)}')`);
            return ext.Promises.promise(resolve => {
                resolve();
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< ControlPanelServiceMock::onSetup");
            });
        },

        /**
         *
         * @see {@link Wincor.UI.Service.BaseServiceMock#onServicesReady}
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         */
        onServicesReady: function($super) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> ControlPanelServiceMock::onServicesReady()");
            return ext.Promises.promise(resolve => {
                $super().then(resolve);
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< ControlPanelServiceMock::onServicesReady");
            });
        },

        /**
         * @see {@link Wincor.UI.Service.BaseServiceMock#initialize}.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super();
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> ControlPanelServiceMock::initialize()");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< ControlPanelServiceMock::initialize");
        }

    });

    return Wincor.UI.Service.ControlPanelServiceMock;
});
