/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
define(["pt-service", "extensions"], function(ptSvc, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.DataService");

    /**
     *
     * @type {Wincor.UI.Diagnostics.LogProvider|*|Wincor.UI.Diagnostics.LogProvider|*}
     * @const
     * @private
     */
    const _logger = Wincor.UI.Diagnostics.LogProvider;

    const META_UTF8 = "PARLIST_UTF8";
    const META_ANSI = "PARLIST_ANSI";

    const VALUE_ARRAY_SEPARATOR = "_||_";
    const VALUE_CHANGED_SEPARATOR = "_WNSEP_";
    const KEY_ARRAY_INDICATOR = "[A]";

    /**
     * The DataService class provides methods to retrieve data-values from the business logic (properties).
     * @class
     * @since 1.0/00
     */
    Wincor.UI.Service.DataService = Class.create(Wincor.UI.Service.PTService/**@lends Wincor.UI.Service.DataService.prototype*/, {

        /**
         * The logical name of this service as used in the {@link Wincor.UI.Service.Provider}.
         * @default DataService
         * @const
         * @type {string}
         */
        NAME: "DataService",

        //HINT: during Class.Create own members are not accessable -> instanciate messages/events in constructor !!!

        /**
         * @ignore
         * @deprecated not necessary
         */
        DATA_VALUES_CHANGED_EVENT: null,

        /**
         * @ignore
         * @deprecated not necessary
         */
        DATA_REQUEST_MESSAGE: null,

        /**
         * Structure containing the registered data keys
         * @class
         */
        DataRegistration: function () {
            /**
             * The registered data keys of the business properties.
             * @type {Array<string>}
             */
            this.keys = [];          //the registered dataKeys

            /**
             * A callback function which is called when the dataChanged event is triggered
             * @type {function}
             */
            this.onUpdate = null;    //this callback is called when the dataChanged event is triggered

            /**
             * If registration is persistent (true), a content-page unload will not remove the registration, false otherwise.
             * @type {boolean}
             */
            this.persistent = false; //if registration is persistent, a content-page unload will not remove the registration
        },

        /**
         * Array containing elements of {@link Wincor.UI.Service.DataService#DataRegistration}.
         * @type {Array}
         */
        dataArray: [], // [DataRegistration1, DataRegistration2, ... ]

        /**
         * Contains the business property keymap.
         * @type {Object}
         */
        businessPropertyKeys: null,

        /**
         * Contains the UI specific property keys.
         * @type {Object}
         */
        UIPropertyKeys: null,

        /**
         * Called automatically as soon as there is an answer to an asynchronous ProTopas request. See {@link Wincor.UI.Service.BaseService#translateResponse}.
         * The response will be translated to the result expected by the requester.
         *
         * @param {object} message    Response object, see {@link Wincor.UI.Service.BaseService#translateResponse}.
         * @returns {*}               Depends on function
         */
        translateResponse: function (message) {
            try {
                let ret;
                _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> DataService::translateResponse('${JSON.stringify(message)}')`);

                //GETKEYS response
                if (message.FWFuncID === 30) {
                    if (message.RC === 0) {
                        /*
                         message.param1 has the keys,   i.e. [key1,   key2, ...]
                         message.param2 has the values, i.e. [value1, value2, ...]
                         message.param3 has the RCs, i.e. [RC1, RC2, ...]

                         If 'key1' looks like 'PropName[A]' (i.e. with [A] at the end)
                         then 'value1' will be a comma-separted list of values.
                         This will be transferred into an array.

                         getKeysResponse will be an object:
                         getKeysResponse["key1"] = value1
                         getKeysResponse["key2"] = value2
                         getKeysResponse["key[A]"] = [value30, value32, value33, ...]
                         ...
                         */
                        let response = {}, key;
                        for(let i in message.param1) {
                            if(message.param1.hasOwnProperty(i)) {
                                key = message.param1[i].toString();
                                //TODO: check RC? use null for invalid keys! same as LocalizeService.
                                //if (message.param3[i].toString() === "0")
                                //then response[key] = message.param2[i];
                                //else response[key] = null;
                                if((key.indexOf(KEY_ARRAY_INDICATOR) === key.length - 3) || //if it ends with [A]
                                    (key.indexOf("[A,") !== -1 && key.indexOf("]") === key.length - 1 )) { //if it looks like [A,x]
                                    response[key] = message.param2[i].split(VALUE_ARRAY_SEPARATOR);
                                } else {
                                    response[key] = message.param2[i];
                                }
                            }
                        }
                        ret = response;
                    } else {
                        ret = {};
                    }
                } else if (message.FWFuncID === 31) {
                    ret = message.RC; // always return rc
                } else {
                    _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `. DataService::onResponse message with FWFuncID '${message.FWFuncID}' and RC = '${message.RC}' will not be evaluated.`);
                }
                
                _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `< DataService::translateResponse returns: ${JSON.stringify(ret)}`);
                return ret;
            }
            catch (e) {
                // Provider writes error-log
                Wincor.UI.Service.Provider.propagateError(this.NAME, this.ERROR_TYPE.RESPONSE, e);
            }
        },

        /**
         * Gets a business property key from the business property key map.
         * @param {String} key the to get the property
         * @returns {String} the property key or an empty string in case of error
         */
        getKey: function(key) {
            if(key in this.businessPropertyKeys) {
                return this.businessPropertyKeys[key];
            }
            else {
                console.error(key + " not in business property key map available.");
                return "";
            }
        },

        /**
         * Get the values of the requested parameters from the business logic.
         * @param {Array<string> | string} keys e.g. single string or ["VAR_MY_HTML_NAME_S", "CUSTOMER_SURNAME", ...]
         * @param {function (Object)} callback
         * @param {function (Object)} onUpdateCallback callback is called when a key was updated.
         * @param {boolean=} persistent true, if the -onUpdateCallback callback function should stay persistent, even a
         *        {@link Wincor.UI.Service.DataService#cleanDataRegistrations} is invoked, false otherwise.
         */
        getValues: function (keys, callback, onUpdateCallback, persistent) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> DataService::getValues(" + keys + ")");

            //BEGIN: check input parameters
            keys = Array.isArray(keys) ? keys : [keys];
            let uniqueKeys = [];
            let errorMessage, len = keys.length;
            for (let i = 0; i < len; ++i) { // must use forward loop, because of order of the keys (e.g. ProFlex4Op)
                if(keys[i] !== void 0 && keys[i] !== null) { //do NOT use '!keys[i]' because empty string is okay, 0 is okay
                    if(!uniqueKeys.includes(keys[i])) {
                        uniqueKeys.push(keys[i]);
                    }
                    else {
                        _logger.error("Warning: DataService::getValues double key detected: " + keys[i]);
                    }
                }
                else {
                    errorMessage = "DataService.getValues() keys contains null or undefined.";
                    break;
                }
            }
            keys = uniqueKeys;

            if (errorMessage) {
                _logger.error(errorMessage);
                callback({}); //call callback with empty object
                Wincor.UI.Service.Provider.propagateError("DataService::getValues", this.ERROR_TYPE.REQUEST);
                return;
            }
            //END: check input parameters

            //build a new TranslateRegistration object and add it to our Array if (and only if!) an onUpdateCallback is given
            if (onUpdateCallback) {
                var dataReg = new this.DataRegistration();
                dataReg.keys = keys;
                dataReg.onUpdate = onUpdateCallback;
                dataReg.persistent = persistent ? persistent : false; // note, persistent arg is optional
                this.dataArray.push(dataReg);
            }

            this.FRM_RESOLVE_REQUEST.FWFuncID = 30;             //DATADICTIONARYEXT_FUNC_GET_KEY_VALUES
            this.FRM_RESOLVE_REQUEST.param1= keys;
            this.FRM_RESOLVE_REQUEST.meta1= [META_ANSI, -1];
            this.FRM_RESOLVE_REQUEST.param2= [];
            this.FRM_RESOLVE_REQUEST.meta2= [META_UTF8, 16000];
            this.FRM_RESOLVE_REQUEST.param3 = [];
            this.FRM_RESOLVE_REQUEST.meta3 = [META_ANSI, 8000];
            this.FRM_RESOLVE_REQUEST.paramUL= 0;

            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `Wincor.UI.Service.DataService.getValues() Request to send: '${JSON.stringify(this.FRM_RESOLVE_REQUEST)}'.`);
            this.FrmResolve(this.FRM_RESOLVE_REQUEST, callback);
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::getValues");
        },

        /**
         * Set the values of the requested parameters to be stored within the business logic.
         * @param {Array<string> | string} keys e.g. a single string or ["VAR_MY_HTML_NAME_S", "CUSTOMER_SURNAME", ...]
         * @param {Array<string> | string} values e.g. a single string or ["cardinsert.html", "Doe", ...]
         * @param {function (number)} callback
         */
        setValues: function (keys, values, callback) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> DataService::setValues(${keys}, ${values})`);

            //BEGIN: check input parameters
            keys = Array.isArray(keys) ? keys : [keys];
            values = Array.isArray(values) ? values : [values];
            let errorMessage;
            if (keys.length !== values.length) {
                errorMessage = "DataService.setValues(): keys and values do not have the same size.";
            }
            else {
                for (let i = 0; i < keys.length; ++i) {
                    if ( keys[i] === undefined || keys[i] === null ) { //do NOT use '!keys[i]' because empty string is okay, 0 is okay
                        errorMessage = "DataService.setValues(): keys contains null or undefined.";
                        break;
                    }
                    if ( values[i] === undefined || values[i] === null ) { //do NOT use '!keys[i]' because empty string is okay, 0 is okay
                        errorMessage = "DataService.setValues(): values contains null or undefined.";
                        break;
                    }
                }
            }

            if (errorMessage) {
                _logger.error(errorMessage);
                callback(-1); //call callback with a RC != 0
                Wincor.UI.Service.Provider.propagateError("DataService::setValues", this.ERROR_TYPE.REQUEST);
                return;
            }
            //END: check input parameters

            this.FRM_RESOLVE_REQUEST.FWFuncID = 31;             //DATADICTIONARYEXT_FUNC_SET_KEY_VALUES
            this.FRM_RESOLVE_REQUEST.param1= keys;
            this.FRM_RESOLVE_REQUEST.meta1= [META_ANSI, -1];
            this.FRM_RESOLVE_REQUEST.param2= values;
            this.FRM_RESOLVE_REQUEST.meta2= [META_UTF8, -1];
            this.FRM_RESOLVE_REQUEST.param3 = [];
            this.FRM_RESOLVE_REQUEST.meta3 = [META_ANSI, 8000];
            this.FRM_RESOLVE_REQUEST.paramUL= 0;

            
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `Wincor.UI.Service.DataService.setValues() Request to send: '${JSON.stringify(this.FRM_RESOLVE_REQUEST)}'.`);
            this.FrmResolve(this.FRM_RESOLVE_REQUEST, callback);
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::setValues");
        },

        /**
         * Clean the {@link Wincor.UI.Service.DataService#dataArray} containing all
         * {@link Wincor.UI.Service.DataService#DataRegistration} structures which has been set by the
         * {@link Wincor.UI.Service.DataService#getValues} method.
         */
        cleanDataRegistrations: function () {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> DataService::cleanDataRegistrations()");
            let newDataArray = [];
            // keep the persistent items in -newDataArray
            for (let i = this.dataArray.length - 1; i >= 0; i--) {
                if(this.dataArray[i].persistent) {
                    // keep the persistent item
                    newDataArray.push(this.dataArray[i]);
                }
            }
            this.dataArray = newDataArray; // empty array or the persistent items kept
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::cleanDataRegistrations");
        },

        /**
         * This method will be triggered when a value of a key has been changed.
         * The changed key will be updated in the DataRegistration structure, which contains all prior requested keys.
         * @param {Array} value e.g. ["VAR_MY_HTML_NAME_S", "CUSTOMER_SURNAME", ...]
         * @private
         */
        onValueChanged: function (value) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> DataService::onValueChanged(${value})`);
            let strValue = Wincor.ConvHexToStr(value);  // values[0] has the message content
            let splitResult = strValue.split(VALUE_CHANGED_SEPARATOR);
            let changedKey  = splitResult[0];
            let newValue = splitResult[1];
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `. DataService::onValueChanged changedKey='${changedKey}', newValue='${newValue}'`);
            for(let i = 0; i < this.dataArray.length; i++) {
                for(let j = 0; j < this.dataArray[i].keys.length; j++) {
                    if(this.dataArray[i].keys[j] === changedKey) {
                        let result = {};  //build the object
                        result[changedKey] = newValue;
                        this.dataArray[i].onUpdate(result);     //call the callback with the object
                    }
                }
            }
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::onValueChanged");
        },

        /**
         * This method will be triggered when an Unicode (WCHAR*) value of a key has been changed.
         * The changed key will be updated in the DataRegistration structure, which contains all prior requested keys.
         * @param {string} value Key + separator + new Value (WCHAR*) e.g. "VIDEO_PEERID_NAME_WNSEP_"
         * @private
         */
        onValueChangedUnicode: function (value) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> DataService::onValueChangedUnicode(${value})`);
            let strValue = value;  // UTF-16 value
            let splitResult = strValue.split(VALUE_CHANGED_SEPARATOR);
            let changedKey  = splitResult[0];
            let newValue = splitResult[1];
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `. DataService::onValueChangedUnicode changedKey='${changedKey}', newValue='${newValue}'`);
            for(let i = 0; i < this.dataArray.length; i++) {
                for(let j = 0; j < this.dataArray[i].keys.length; j++) {
                    if(this.dataArray[i].keys[j] === changedKey) {
                        let result = {};  //build the object
                        result[changedKey] = newValue;
                        this.dataArray[i].onUpdate(result);     //call the callback with the object
                    }
                }
            }
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::onValueChangedUnicode");
        },

        /**
         * Reads the <i>UIPropertyKeyMap.json</i>, the <i>BusinessPropertyKeyMap.json</i> and <i>BusinessPropertyCustomKeyMap.json</i> and stores the content in
         *
         * @param {object} message See {@link Wincor.UI.Service.BaseService#onSetup}
         * @returns {Promise}
         * @lifecycle service
         * @see {@link Wincor.UI.Service.BaseService#onSetup}
         */
        onSetup: function(message) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> DataService::onSetup('${JSON.stringify(message)}')`);
            const self = this;
            const fileUIKeyMap = "../servicedata/UIPropertyKeyMap.json";
            const fileBusinessKeys = "../servicedata/BusinessPropertyKeyMap.json";
            const fileBusinessCustomKeys = "../servicedata/BusinessPropertyCustomKeyMap.json";
            return ext.Promises.promise(function(resolve, reject) {
                ext.Promises.Promise.all([self.retrieveJSONData(fileBusinessKeys), self.retrieveJSONData(fileBusinessCustomKeys), self.retrieveJSONData(fileUIKeyMap)]).then(dataArray=> {
                    delete dataArray[1]["//"]; // remove possible comment
                    self.businessPropertyKeys = Object.assign(dataArray[0], dataArray[1]); // standard keys with custom specific ones
                    self.UIPropertyKeys = dataArray[2];
                    resolve();
                    _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::onSetup");
                }).catch(e=> {
                    _logger.error(`* importReference error getting ${fileBusinessKeys} or ${fileBusinessCustomKeys}`);
                    reject(e);
                });
            });
        },

        /**
         * Registers for DataDictionary events for property changes.
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         * @see {@link Wincor.UI.Service.BaseService#onServicesReady}
         */
        onServicesReady: function ($super) {
            return ext.Promises.promise(resolve => {
                _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> DataService::onServicesReady()");
                function registerCallback(message) {
                    _logger.LOG_ANALYSE && _logger.log(_logger.LOG_ANALYSE, "* DataService::onServicesReady (registerCallback): RC: " + message.RC);
                }

                this.serviceProvider.EventService.registerForEvent(666, "CCDatDic", this.onValueChangedUnicode.bind(this), registerCallback.bind(this), "UTF-8", true);
                this.serviceProvider.EventService.registerForEvent(667, "CCDatDic", this.onValueChangedUnicode.bind(this), registerCallback.bind(this), "UTF-16", true);
                const viewService = this.serviceProvider.ViewService;
                viewService.registerForServiceEvent(viewService.SERVICE_EVENTS.VIEW_CLOSING, this.cleanDataRegistrations.bind(this), true);
                viewService.registerForServiceEvent(viewService.SERVICE_EVENTS.SHUTDOWN, this.cleanDataRegistrations.bind(this), true);
                _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::onServicesReady");

                $super().then(resolve);
            });
        },

        /**
         * This function can be called from outside the UI to retrieve arbitrary viewModel data as a property!
         * The functionality relies on a naming convention regarding the attribute name and context to access.
         * Naming convention is defined as follows:
         * GUIINSTANCENAME_ATTRIBUTENAME_CONTEXTNAME, where "GUIINSTANCENAME_" will be cut by gui.dll and not arrive here!
         * This automatic value resolution via naming convention heavily relies on a correct case-sensitive spelling, therefore mappings can be
         * used within the file "core/servicedata/businessPropertyKeyMap.json"
         * For CONTEXTNAME the "observableAreaId" of a corresponding viewModel can be given so that a valid name could be:
         * "GUIAPP_flexHeader.date". Service attributes can also be accessed if exposed via proxy using the servicename as context like:
         * "ViewService.viewContext.viewConfig"
         * The result will be send back to the native part as response containing the stringified value of the attribute or null if it does not exist.
         * @param {object} message message containing request-data
         * @param {string} message.propertyName contains the propertyName - has to follow the above naming convention
         * @param {string} message.propertyValue will be set to the value of the property, unchanged if not found...
         * @return {string} value for internal requests
         */
        getPropertyString: function(message) {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `> DataService::getPropertyString(${JSON.stringify(message)})`);
            let name = message.propertyName;
            let value = null;
            let ret;
            // try to get it from mapping, otherwise try to disassemble directly
            name = this.UIPropertyKeys[name] || name;
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, ". after mapping: " + name);

            // now we have: flexHeader.date -> first index is context, next attributes might be more than one for nested objects... walk structure hierarchy
            let parts = name.split(".");
            let contextName = parts.splice(0, 1)[0]; // pop first entry as context
            let context = Wincor.UI.Service.Provider[contextName];

            if (!context) {
                // it wasn't a service, look for specials
                if(contextName.indexOf("Wincor") === 0) {
                    context = Wincor;
                } else if(contextName.indexOf("window") === 0) {
                    context = window.frames[0];
                }
            }

            // If there is a viewset switch active, we won't have "Content" available!
            if (!context && Wincor.UI.Content && Wincor.UI.Content.ViewModelContainer) {
                // at last try if there is an observable area / vm with this name...
                context = Wincor.UI.Content.ViewModelContainer.getById(contextName);
            }

            if (context) {
                value = parts.reduce(function(c, a) {
                    if (c && a in c) {
                        try {
                            if(typeof c[a] === "function" && "__ko_proto__" in c[a]) {
                                return c[a]();
                            } else {
                                return c[a];
                            }
                        } catch(e) {
                            _logger.error(`DataService::getPropertyString exception during attribute evaluation: '${e.message}'`);
                        }
                    }
                    return void 0;
                }, context);
            }

            if (value !== void 0 && value !== null) {
                message.propertyValue = value.toString();
                ret = this.REQUEST_RESPONSE_OK;
                this.sendResponse(message, ret);
            } else {
                let err = name;
                if (message.propertyName !== name) {
                    err = message.propertyName+"' aka '" + name ;
                }
                _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `DataService::getPropertyString attribute could not be found for: '${err}'`);
                ret = "-1";
                this.sendResponse(message, ret);
            }

            if (typeof value === "object") {
                try {
                    value = JSON.stringify(value);
                } catch(e) {
                    value = value.toString();
                }
            }

            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `< DataService::getPropertyString returns ${ret} data: ${JSON.stringify(message)}`);
            return value;
        },

        /**
         * Initializes the member of this class.
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         * @see {@link Wincor.UI.Service.PTService#initialize}.
         */
        initialize: function ($super) {
            $super(); // Invoke superclass's initialize first
            this.businessPropertyKeys = {};
            this.UIPropertyKeys = {};
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> DataService::initialize()");
            this.FRM_RESOLVE_REQUEST.service = this.NAME;
            this.FRM_RESOLVE_REQUEST.FWName = "CCDatDic";
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< DataService::initialize");
        }
    });
    return Wincor.UI.Service.DataService;
});