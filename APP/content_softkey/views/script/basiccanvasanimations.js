/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
/**
 * The basiccanvasanimations module provides some functionality to draw a rectangle button effect around each button canvas.
 * @module basiccanvasanimations
 * @since 1.0/00
 */
define(["jquery", "extensions", "ui-content", "config/Config"], function(jQuery, ext, content, config) {
	console.log('AMD:basiccanvasanimations');

    const _logger = Wincor.UI.Service.Provider.LogProvider;

    /**
     * Definitions for the valid directions. AUTOALIGN will calculate if the middle of the element is on the left or right side of the screen and draw accordingly.
     * @const
     * @type {{AUTOALIGN: string, LEFT: string, RIGHT: string}}
     */
    const DIRECTIONS = {
        AUTOALIGN: "AUTOALIGN",
        LEFT: "LEFT",
        RIGHT: "RIGHT"
    };

    /**
     * The direction for drawing see {@see DIRECTIONS}
     * @type {string}
     */
    let _direction = config.BORDER_DRAWING_DIRECTION.toUpperCase();

    /**
     * If set to true by config, animations will be started, otherwise elements will directly be shown
     * @type {boolean}
     */
    const _borderAnimationsOn = config.BORDER_DRAWING_ON;

    /**
     * Number used to calculate animationTime in correlation to width+height of element
     * Eike: I think the deferral value applies to the number of GPU frames used to draw the button.
     * @type {number}
     */
    const _borderDrawingDeferral = (0 <= config.BORDER_DRAWING_DEFERRAL && 150 >= config.BORDER_DRAWING_DEFERRAL) ? config.BORDER_DRAWING_DEFERRAL : 10;

    const _buttonFadeInTime = (0 <= config.BUTTON_FADE_IN_TIME && 2000 >= config.BUTTON_FADE_IN_TIME) ? config.BUTTON_FADE_IN_TIME : 200;

    const cs = Wincor.UI.Service.Provider.ConfigService;
    const activationTimeout = !content.designMode ? cs.configuration[cs.configuration.instanceName].TimeoutActivate : 20000;
    const SHORT_BEFORE_ACTIVATION_TIMEOUT = !activationTimeout ? 20000 : 0.5 * activationTimeout;


    function drawingReady(jCan) {
        return ext.Promises.promise(function(resolve, reject) {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `> basiccanvasanimations::drawingReady(${jCan.next().attr('id')})`);

            //After fading in the button/element (with _buttonFadeInTime) to its original opacity-value, we instantly remove the canvas.
            //Before UI 2.0/00 the canvas was faded out at the same time as the button faded in, but there was visually no difference to this solution.

            //As easing function (see http://ricostacruz.com/jquery.transit -> easing) we use 'in' instead of using no easing function.
            //No easing function has the effect that the button looks ready very quickly, but the fading is only half done!
            //With the easing 'in' function this "readiness look" of the button fits linear to the fading time.
            //To check this, just delete the 'in' function and use _buttonFadiInTime=10000: you will see that the button will look okay after approx. 5sec, but the fade in takes 10sec!
            const jElement = jCan.next();
            jElement.transition({'opacity': ''}, _buttonFadeInTime, 'in', function() {  //using empty string means the original CSS value
                //remove the canvas finally. TODO: write down why we have to remove it
                _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `basiccanvasanimations::drawingReady canvas of ${jCan.next().attr('id')} `);
                jCan.remove();
                jCan = null;
                resolve();
                _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `basiccanvasanimations::drawingReady resolved`);
            });

            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "< basiccanvasanimations::drawingReady2");
        });
    }

    return /** @alias module:basiccanvasanimations */ {
        /**
         * Draws an animated rectangle on a canvas. This function shall only be called by 'doStartDrawing()' and is therefore declared as private.
         *
         * @param {object} jCanvas      The jQuery canvas object
         * @param {number} _x           Upper left corner x
         * @param {number} _y           Upper left corner y
         * @param {number} _width       Width of the rectangle
         * @param {number} _height      Height of the rectangle
         * @param {string} _lineStroke  Stroke style of the line
         * @param {number} _animSpeed   Speed of the animation
         * @param {string} _direction   Direction of the animation
         * @returns {promise}
         * @private
         */
        drawAnimatedRectangle: function(jCanvas, _x, _y, _width, _height, _lineStroke, _animSpeed, _direction) {
            //console.log("drawAnimatedRectangle(" + _x + ", " + _y + ", " + _width + ", " + _height, ", " + _animSpeed + ", " + _direction + ")");
            return ext.Promises.promise(function(resolve, reject) {
                try {
                    _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `> basiccanvasanimations::drawAnimatedRectangle(canvas of ${jCanvas.next().attr('id')}, x=${_x}, y=${_y}, w=${_width}, h=${_height}, linestroke=${_lineStroke}, animSpeed=${_animSpeed}, dir=${_direction})`);
                    if (_animSpeed <= 0 || _height <= 0 || _width <= 0) {
                        resolve(jCanvas);
                        _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `< basiccanvasanimations::drawAnimatedRectangle invalid speed or size... resolving`);
                        return;
                    }

                    let ctx = jCanvas[0].getContext("2d");
                    if(_direction === DIRECTIONS.RIGHT) {
                        //we translate the context by half a pixel to get razor-sharp lines, see also http://stackoverflow.com/questions/18748001/lineto-method-in-a-for-loop
                        ctx.translate(0.5, 0.5);
                        //if we do not do this, the lines will get blurred, because of antialising
                    }
                    else {
                        //This draws from the upper right to the left.
                        //Instead of calculating a different path, we can calculate the same path, but we translate starting point to the upper right of the canvas
                        //and we mirror the context on the y-axis with scale(-1, 1)
                        ctx.translate(-0.5 + _width, 0.5); //it must be -0.5+width, not 0.5+width: because of the follwing mirroring by scale(-1, 1) the right border would not be drawn otherwise (you can test it with idle loop in touch layout)
                        ctx.scale(-1, 1);

                    }

                    ctx.strokeStyle = _lineStroke;
                    ctx.lineWidth = 1;

                    /*

                    *----*----*----*----*----*--*
                    |                           |
                    |                           |
                    |                           |
                    |                           |
                    +                           +
                    +----*----*----*----*----*--+
                    _width = 29
                    _height = 7
                    _animSpeed = 5
                    --> fullFactorX = 5
                    --> remainingPixelX = 4
                    --> fullFactorY = 1
                    --> remainingPixelY = 2
                    --> lastPixelX = 28
                    --> path1 = (0,0),
                                (5,0), (10,0), (15,0), (20,0), (25,0), (28,0),  //these elements will become "pathTmpX" later
                                (28,5), (28,6)                                  //these elements will become "pathTmpY" later
                    --> path2 = (0,0),
                                (0,5), (0,6),                                   //these elements will be calculated from pathTmpY
                                (5,6), (10,6), (15,6), (20,6), (25,6), (28,6)   //these elements will be calculated from pathTmpX


                     *----*----*----*----*----*---*
                     |                            |
                     |                            |
                     |                            |
                     +----*----*----*----*----*---+
                     _width = 30
                     _height = 5
                     _animSpeed = 5
                     --> fullFactorX = 6
                     --> remainingPixelX = 0
                     --> fullFactorY = 1
                     --> remainingPixelY = 0
                     --> lastPixelX = 29
                     --> path1 = (0,0),
                                 (5,0), (10,0), (15,0), (20,0), (25,0), (29,0),
                                 (29,5)


                     *----*----*----*----*----*----*
                     |                             |
                     |                             |
                     |                             |
                     |                             |
                     +----*----*----*----*----*----+
                     _width = 31
                     _height = 6
                     _animSpeed = 5
                     --> fullFactorX = 6
                     --> remainingPixelX = 1
                     --> fullFactorY = 1
                     --> remainingPixelY = 1
                     --> lastPixelX = 30
                     --> path1 = (0,0),
                                (5,0), (10,0), (15,0), (20,0), (25,0), (30,0),
                                (30,6)

                     */


                    //Initialise with the starting point in the top left corner.
                    let path1 = [
                        {
                            "x": _x,
                            "y": _y
                        }
                    ];

                    const fullFactorX = Math.floor(_width / _animSpeed); //floors in wrong direction if width < 0, but we already have set it to positive value above
                    const remainingPixelX = _width % _animSpeed;

                    const fullFactorY = Math.floor(_height / _animSpeed);
                    const remainingPixelY = _height % _animSpeed;

                    const lastPixelX = _x + _animSpeed * fullFactorX + remainingPixelX - 1;
                    const lastPixelY = _y + _animSpeed * fullFactorY + remainingPixelY - 1;

                    //console.log("drawAnimatedRectangle calc = (" + fullFactorX + ", " + remainingPixelX + ", " + fullFactorY + ", " + remainingPixelY + ")");

                    //to the right....
                    for(let i = 1; i <= fullFactorX; i++) {
                        path1.push({
                            "x": _x + _animSpeed * i,
                            "y": _y
                        });
                    }
                    if(remainingPixelX === 0 || remainingPixelX === 1) {
                        path1.pop(); //like seen in the second or third example: (30,0) was pushed onto the array, but that's either too far (second example) or will be added anyway in the next step:
                    }

                    path1.push({
                            "x": lastPixelX,
                            "y": _y
                        }
                    );

                    const timesX = path1.length - 1;

                    //...then down
                    for(let i = 1; i <= fullFactorY; i++) {
                        path1.push({
                            "x": lastPixelX,
                            "y": _y + _animSpeed * i
                        });
                    }
                    if(remainingPixelY === 0 || remainingPixelY === 1) {
                        path1.pop(); //like seen in the second example: (30,0) was pushed onto the array, but that's not needed
                    }
                    path1.push({
                            "x": _x + lastPixelX,
                            "y": lastPixelY
                        }
                    );
                    //Now path1 holds all points on the horizontal axis and on the vertical axis, which will be used during drawing

                    // *** Instead of using the same algorith for path2, we just use the already calculated points from path1 and 'translate' them
                    // *** Advantage: should we every change the algorithm for path1, the points on path2 will automatically be updated accordingly

                    //console.log("drawAnimatedRectangle path1=(" + JSON.stringify(path1) + ")");

                    let path2 = [];
                    path2.push(path1[0]);

                    let pathTmpX = JSON.parse(JSON.stringify(path1.slice(1, timesX + 1))); //var pathTmpX = path1.slice(1, timesX + 1);  will not make a copy of the contained objects!

                    for(let i = 0; i < pathTmpX.length; i++) {
                        pathTmpX[i].y = lastPixelY;
                    }

                    let pathTmpY = JSON.parse(JSON.stringify(path1.slice(timesX + 1, path1.length)));

                    for(let i = 0; i < pathTmpY.length; i++) {
                        pathTmpY[i].x = _x;
                    }

                    path2 = path2.concat(pathTmpY);
                    path2 = path2.concat(pathTmpX);

                    const pathLen = path1.length; // is the same for path1 and path2
                    let startIndex = 0;

                    _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "basiccanvasanimations::drawAnimatedRectangle2");

                    function drawRectangle() {
                        try {
                            ctx.moveTo(path1[startIndex].x, path1[startIndex].y);
                            ctx.lineTo(path1[startIndex + 1].x, path1[startIndex + 1].y);

                            ctx.moveTo(path2[startIndex].x, path2[startIndex].y);
                            ctx.lineTo(path2[startIndex + 1].x, path2[startIndex + 1].y);

                            ctx.stroke();

                            startIndex++;
                            if(startIndex >= pathLen - 1) {
                                _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `basiccanvasanimations::drawAnimatedRectangle:drawRectangle finished for canvas of ${jCanvas.next().attr("id")} - startIndex: ${startIndex}`);
                                path1 = pathTmpX = pathTmpY = path2 = ctx = startIndex = null;
                                resolve(jCanvas);
                            } else {
                                // avoid modulo calculations if LOG_DETAIL is off
                                if(_logger.LOG_DETAIL && startIndex % 5 === 0) {
                                    _logger.log(_logger.LOG_DETAIL, `basiccanvasanimations::drawAnimatedRectangle:drawRectangle requestAnimationFrame drawing for canvas of ${jCanvas.next().attr('id')} startIndex/pathLen-1: ${startIndex}/${pathLen - 1}`);
                                }
                                requestAnimationFrame(drawRectangle);
                            }
                        } catch(e) {
                            // we don't care for errors here... pass
                            _logger.error(`basiccanvasanimations::drawAnimatedRectangle ${e.message}\n${e.stack}`);
                            resolve(jCanvas);
                        }
                    }

                    _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "< basiccanvasanimations::drawAnimatedRectangle");
                    drawRectangle();
                } catch(e) {
                    _logger.error(`basiccanvasanimations::drawAnimatedRectangle ${e.message}\n${e.stack}`);
                    reject();
                }
            });
        },

        /**
         * Starts the border drawing.
         * @param {jQuery=} $elementToDraw      A certain jQuery element object to draw the border of.
         *                                      If the argument isn't given the method tries to find all potential rectangle canvas elements to start the drawing for.
         * @returns {Promise}                   Resolves when the drawing has finished.
         */
        doStartDrawing: function($elementToDraw) {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "> basiccanvasanimations::doStartDrawing");
            let drawingPromises = [];
            if(_borderAnimationsOn) {
                const windowWidth = jQuery(window).width();
                let allCanvases = !$elementToDraw ? jQuery("#applicationHost").find(".rectangleDrawingCanvas") : $elementToDraw.find(".rectangleDrawingCanvas");
                const len = allCanvases.length;
                let x, y, $canvas, canvas, next, width, height, speed, jCanvasOffset, borderTopColor, lineColor, isInViewPort = true, rect;

                _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, ". basiccanvasanimations::doStartDrawing number of '.rectangleDrawingCanvas' to draw=" + len);

                if(!(_direction in DIRECTIONS)) {
                    _direction = DIRECTIONS.AUTOALIGN;
                }
                for(let i = 0; i < len; i++) {
                    x = y = 0;
                    $canvas = jQuery(allCanvases[i]);
                    canvas = $canvas[0];
                    next = $canvas.next();
                    // check whether element is part of current viewport, this means not that the whole element must be inside. Partly is sufficient.
                    // Only in this case a drawing is reasonable.
                    rect = canvas.getBoundingClientRect();
                    if(rect.width === 0 || rect.height === 0) {
                        _logger.LOG_ERROR && _logger.log(_logger.LOG_ERROR, `basiccanvasanimations: canvas cannot be drawn for item ${next[0].id} because of zero size`);
                        next.css("opacity", ""); // setting empty string restores the original CSS value
                        // remove class 'rectangleDrawingCanvas' from canvas here, because next item is of invalid size
                        $canvas.removeClass("rectangleDrawingCanvas");
                        continue;
                    }

                    isInViewPort = rect.top >= 0 && rect.left >= 0 && rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
                                   rect.left <= (window.innerWidth || document.documentElement.clientWidth);

                    if(!isInViewPort) {
                        // the upper check only considers partly visible canvases shifting out on the right or the bottom...
                        // check also elements hanging around partly on the left and upper edge
                        isInViewPort = rect.bottom >= 0 && rect.left <= (window.innerWidth || document.documentElement.clientWidth) &&
                                       rect.top <= (window.innerHeight || document.documentElement.clientHeight) && rect.right >= 0;
                    }
                    // checking for "out of view port", hidden or disabled elements
                    if(!isInViewPort || next.css("display") === "none" || next.css("visibility") === "hidden" || next.attr("disabled") === "disabled") {
                        next.css("opacity", ""); // setting empty string restores the original CSS value
                        // do not remove class 'rectangleDrawingCanvas' from canvas here in order to draw buttons after the view has been activated (e.g. submenus)
                        continue;
                    }
                    // prevent from next time the jQuery.find would get the canvas again, because its not removed from the DOM so far (drawing may be in process for that canvas)
                    $canvas.removeClass("rectangleDrawingCanvas");

                    width = next.innerWidth() + 2;
                    height = next.innerHeight() + 2;

                    //console.log("drawAnimatedRectangle width/height = " + width + "/" + height);

                    // Set canvas size! This is not the same as the width and height in css!
                    canvas.style.width = width + 1;
                    canvas.style.height = height + 1 ;
                    canvas.width = width + 1;
                    canvas.height = height + 1;

                    jCanvasOffset = $canvas.offset();
                    if(jCanvasOffset.left === 0) { // if left is 0, move another px left to hide left border drawing
                        $canvas.offset({'left': -1});
                    }

                    let currentDirection = _direction;
                    // check if current canvas has to be drawn from left or right
                    if(_direction === DIRECTIONS.AUTOALIGN) {
                        // draw to the left
                        if(jCanvasOffset.left + (width / 2) >= (windowWidth / 2)) {
                            currentDirection = DIRECTIONS.LEFT;
                        } else {
                            currentDirection = DIRECTIONS.RIGHT;
                        }
                    }

                    // auto line-color comes from top-border color of the corresponding element
                    //var borderTopColor = jCanvas.css("color");
                    borderTopColor = next.css("border-top-color");
                    lineColor = borderTopColor ? borderTopColor : "#00ffff";

                    if(!canvas.id) {
                        canvas.id = `__generatedCanvasIdTemp${i}`;
                    }
                    //_logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "basiccanvasanimations::doStartDrawing CANVAS " + canvas.id + " " + lineColor + "  x/y | width/height " + x + "/" + y + " - " + width + "/" + height + "   speed=" + speed);

                    //Before 2.0/00 we used
                    //  speed = Math.round(Math.abs((width + height) / _borderDrawingDeferral));
                    //but because of Math.round you got different rounding errors for different button sizes.
                    //As an effect, buttons of different sizes were not painted at the same speed on one page.
                    speed = Math.abs((canvas.width + canvas.height) / _borderDrawingDeferral);
                    let p = this.drawAnimatedRectangle($canvas, x, y, canvas.width, canvas.height, lineColor, speed, currentDirection);
                    const c = $canvas;
                    const id = $canvas && $canvas.next() && $canvas.next().attr("id");
                    drawingPromises.push(p
                        .timeout(SHORT_BEFORE_ACTIVATION_TIMEOUT)
                        .catch((e) => {
                            c && c.next() && c.next().css("opacity", "");
                            _logger.error(`basiccanvasanimations::doStartDrawing caught error for ${id}:\n${e.message}\n${e.stack}`);
                        })
                        .then(() => {
                            drawingReady(c);
                        })
                        .catch((e) => {
                            c && c.next() && c.next().css("opacity", "");
                            _logger.error(`basiccanvasanimations::doStartDrawing caught error for ${id} after forcing drawingReady:\n${e.message}\n${e.stack}`);
                        })
                    );
                }
            }
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "< basiccanvasanimations::doStartDrawing");
            return ext.Promises.Promise.all(drawingPromises);
        }
    };
});
