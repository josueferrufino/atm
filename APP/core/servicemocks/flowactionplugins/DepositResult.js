/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

/**
 * @module flowactionplugins/DepositResult
 */
define(["extensions", "../flowactionplugins/cashHelper"], function(ext, cashHelper) {
    "use strict";
    console.log("AMD:DepositResult");

    const _dataService = Wincor.UI.Service.Provider.DataService;

    return {

        /**
         * Emulates a business logic step for preparation purpose.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // function: the current view key (corresponding this plug-in name)</li>
         * <li>viewConfig      // object: Configuration of the view (ViewMapping.json)</li>
         * <li>container       // object: ViewModelContainer</li>
         * <li>serviceProvider // object: a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved when the action is ready or rejected on any error.
         */
        onPrepare: function(context) {
            return ext.Promises.promise(async (resolve, reject) => {
                try {
                    await cashHelper.updateDepositCommandStates("0", "0");
                    resolve();
                }
                catch(e) {
                    reject("FlowAction plug-in DepositResult::onPrepare has been failed " + e);
                }

            });
        },

        /**
         * Emulates a business logic flow action when running: Sends an event when deposit retract 'ROLLBACKL3'.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // function: the current view key (corresponding this this plug-in name)</li>
         * <li>config          // object: Configuration of Config.js</li>
         * <li>container       // object: ViewModelContainer</li>
         * <li>serviceProvider // object: a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved when the action is ready or rejected on any error.
         */
        onActivateReady: function(context) {
            return ext.Promises.promise((resolve, reject) => {
                try {
                    cashHelper.resetTrayIOReason();
                    resolve();
                } catch(e) {
                    reject("FlowAction plug-in DepositResult::onActivateReady has been failed " + e);
                }
            });
        },

        /**
         * Emulates a business logic flow action: Sets the CCHPFW_PROP_APPROVAL_AMOUNT.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // the current view key (corresponding this this plug-in name)</li>
         * <li>currentFlow     // the current flow</li>
         * <li>action          // the current action (customer selection)</li>
         * <li>config          // Configuration of Config.js</li>
         * <li>container       // ViewModelContainer</li>
         * <li>serviceProvider // a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved (may argument a new view key destination) when the action is ready or rejected on any error.
         */
        onGuiResult: function(context) {
            return ext.Promises.promise((resolve, reject) => {
               try {
                   if(context.action === "CONFIRM_DEPOSIT") {
                       cashHelper.getDepositL4Amount().then(denom => {
                           return denom;
                       }).then(denom => {
                           return _dataService.setValues(["CCHPFW_PROP_APPROVAL_AMOUNT"], [(parseInt(denom.value) * parseInt(denom.count)).toString()], null);
                       }).then(() => {
                           resolve();
                       })
                   } else {
                       resolve();
                   }
               } catch(e) {
                   reject("FlowAction plug-in DepositResult::onGuiResult has been failed " + e);
               }
           });
        }
    };
});
