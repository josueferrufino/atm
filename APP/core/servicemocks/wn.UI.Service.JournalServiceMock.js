/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["jquery", "extensions", "servicemocks/wn.UI.Service.BaseServiceMock"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.JournalServiceMock");

    /**
     * The JournalServiceMock simulates the journal, currently it only implements the interface, but the functions do not do anything.
     * @class
     */
    Wincor.UI.Service.JournalServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock/**@lends Wincor.UI.Service.JournalServiceMock.prototype*/, {

        /**
         * "JournalServiceMock" - the logical name of this service as used in the service-provider
         * @const
         * @type {string}
         */
        NAME: "JournalService",

        /**
         * Message number defines.
         * These defines are currently used by ProFlex4 UI product. There is an offset of 50 to other UI instances, in which 20 messages of 4 UI instances are reserved.
         * The offsets are added automatically regarding the appropriate instance.
         * GUIAPP (520000-520019)
         * GUIDM  (520050-520069)
         * GUISOP (520100-520119)
         * GUIxx  (520150-520169)
         * For project specific journal messages please use 520000 with an offset of 30-49
         */
        MSG_NUMBERS: {
            MSG_VIEW_DISPLAY: 520000,
            MSG_AJAX_REQUEST: 520001,
            MSG_BROWSER_VERSION: 520002,
            MSG_VIEW_ACTIVATED: 520003,
            MSG_VIEW_INTERACTION: 520010,
            MSG_VIEW_RESULT: 520011
        },
    
    
        /**
         * This method is called by the {@link Wincor.UI.Service.Provider#propagateError} if an error occurred in any service. It logs the error to the console.
         *
         *
         * @param {String} serviceName  The name of this service.
         * @param {String} errorType    As defined in {@link Wincor.UI.Service.BaseService#ERROR_TYPE}.
         */
        onError: function(serviceName, errorType) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> JournalServiceMock::onError(" + serviceName + ", " + errorType + ")");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< JournalServiceMock::onError");
        },

        /**
         * Write a journal entry.
         * @param messageID number, ID of journal message
         * @param callback reference to a function receiving the return code as a parameter
         * @param [arguments] optional arguments for journal message
         * @example
         * // use of CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER method of CCJournal framework
         * // uses ProFlex4/Op registry config:
         * //   [HKEY_LOCAL_MACHINE\SOFTWARE\Wincor Nixdorf\ProTopas\CurrentVersion\JOURNAL\TOPMSG]
         * //     "MSG1001"="=====================================================================================#NL#"
         * // output in *.jrn file:
         * //   =====================================================================================
         * write(1001);
         * @example
         * // use of CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER_WITH_ARGS method of CCJournal framework
         * // uses ProFlex4/Op registry config:
         * //   [HKEY_LOCAL_MACHINE\SOFTWARE\Wincor Nixdorf\ProTopas\CurrentVersion\JOURNAL\TOPMSG]
         * //     "MSG1015"=" @005   @001 <Application> Application state is: #1# (#2#)  #3# #NL#"
         * // output in *.jrn file:
         * //    13:45:12   1015 <Application> Application state is: 111 (222)  333
         * write(1015, null, 111, 222, "333");
         */
        write: function(messageID, callback) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> JournalServiceMock::write(messageID:" + messageID + ", ...)");
            this.callbackCaller(callback);
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< JournalServiceMock::write");
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         *
         * @param {object} message      See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         * @returns {Promise}
         * @lifecycle service
         */
        onSetup: function(message) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> JournalServiceMock::onSetup('" + JSON.stringify(message) + "')");
            return ext.Promises.promise(function(resolve, reject) {
                resolve();
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< JournalServiceMock::onSetup");
            }.bind(this));
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onServicesReady}
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         */
        onServicesReady: function($super) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> JournalServiceMock::onServicesReady()");
            return ext.Promises.promise(function(resolve, reject) {
                $super().then(resolve);
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< JournalServiceMock::onServicesReady");
            }.bind(this));
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#initialize}.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super();
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> JournalServiceMock::initialize()");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< JournalServiceMock::initialize");
        }

    });

    return Wincor.UI.Service.JournalServiceMock;
});
