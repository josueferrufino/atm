/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["jquery", "pt-service", "extensions"], function(jQuery, ptSvc, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.JournalService");

    /**
     *
     * @type {Wincor.UI.Diagnostics.LogProvider|*|Wincor.UI.Diagnostics.LogProvider|*}
     * @private
     */
    const _logger = Wincor.UI.Diagnostics.LogProvider;

    /**
     * The class JournalService has a collection of routines to support journaling.
     * @class
     */
    Wincor.UI.Service.JournalService = Class.create(Wincor.UI.Service.PTService/**@lends Wincor.UI.Service.JournalService.prototype*/, {

        /**
         * "JournalService" - the logical name of this service as used in the service-provider
         * @const
         * @type {string}
         */
        NAME: "JournalService",

        /**
         * Stores the configuration for the JournalService, currently only the journalOffset number, which depends on the current instance.
         * @type {object}
         * @example
         * "config": {"journalOffset": 0}
         */
        config: {},

        /**
         * Message number defines.
         * These defines are currently used by the ProFlex4 UI product. There is an offset of 50 to other UI instances, in which 20 messages of 4 UI instances are reserved.
         * The offsets are added automatically regarding the appropriate instance.
         * ```
         * GUIAPP (520000-520019)
         * GUIDM  (520050-520069)
         * GUISOP (520100-520119)
         * GUIxx  (520150-520169)
         * ```
         * For project specific journal messages please use 520000 with an offset of 30-49
         * @enum {number}
         */
        MSG_NUMBERS: {
            MSG_VIEW_DISPLAY: 520000,
            MSG_AJAX_REQUEST: 520001,
            MSG_BROWSER_VERSION: 520002,
            MSG_VIEW_ACTIVATED: 520003,
            MSG_VIEW_INTERACTION: 520010,
            MSG_VIEW_RESULT: 520011
        },

        /**
         * @deprecated
         * @private
         */
        configDefault: null,

        /**
         * Called automatically as soon as there is an answer to an asynchronous ProTopas request. See {@link Wincor.UI.Service.BaseService#translateResponse}.
         *
         * For every request it returns only the return code as number.
         *
         * @param {object} message    Response object, see {@link Wincor.UI.Service.BaseService#translateResponse}.
         * @returns {Number}          The ProTopas return code of the request, typically 0 in case of success.
         */
        translateResponse: function(message) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> JournalService::translateResponse('${JSON.stringify(message)}')`);
            let ret;
            if (message.FWFuncID === 1) {          //CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER
                ret = message.RC;
            }
            else if (message.FWFuncID === 9) {     //CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER_WITH_ARGS
                ret = message.RC;
            }
            else {
                _logger.error("Wincor.UI.Service.JournalService(onResponse): unknown function");
            }
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `< JournalService::translateResponse returns ${ret}`);
            return ret;
        },

        /**
         * Write a journal entry.
         * @param {string} messageID number, ID of journal message
         * @param {function} callback reference to a function receiving the return code as a parameter
         * @param {*=} [arguments] optional arguments for journal message
         * @example
         * // use of CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER method of CCJournal framework
         * // uses ProFlex4/Op registry config:
         * //   [HKEY_LOCAL_MACHINE\SOFTWARE\Wincor Nixdorf\ProTopas\CurrentVersion\JOURNAL\TOPMSG]
         * //     "MSG1001"="=====================================================================================#NL#"
         * // output in *.jrn file:
         * //   =====================================================================================
         * write(1001);
         * @example
         * // use of CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER_WITH_ARGS method of CCJournal framework
         * // uses ProFlex4/Op registry config:
         * //   [HKEY_LOCAL_MACHINE\SOFTWARE\Wincor Nixdorf\ProTopas\CurrentVersion\JOURNAL\TOPMSG]
         * //     "MSG1015"=" @005   @001 <Application> Application state is: #1# (#2#)  #3# #NL#"
         * // output in *.jrn file:
         * //    13:45:12   1015 <Application> Application state is: 111 (222)  333
         * write(1015, null, 111, 222, "333");
         */
        write: function(messageID, callback) {
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, `> JournalService::write(messageID:${messageID}+offset(${this.config.journalOffset}), ...)`);

            if(messageID) {
                messageID += this.config.journalOffset;
                // check for optional params
                _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `. arguments.length: ${arguments.length}`);
                if(arguments.length < 3){
                    this.FRM_RESOLVE_REQUEST.FWFuncID   = 1;              // CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER
                    this.FRM_RESOLVE_REQUEST.param1     = messageID;
                    this.FRM_RESOLVE_REQUEST.meta1      = ["LONG", 0];
                    this.FRM_RESOLVE_REQUEST.param2     = "";
                    this.FRM_RESOLVE_REQUEST.meta2      = ["NULL", 0];
                    this.FRM_RESOLVE_REQUEST.param3     = "";
                    this.FRM_RESOLVE_REQUEST.meta3      = ["NULL", 0];
                    this.FRM_RESOLVE_REQUEST.paramUL    = 0;

                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `. request to send: '${JSON.stringify(this.FRM_RESOLVE_REQUEST)}'.`);
                    this.FrmResolve(this.FRM_RESOLVE_REQUEST, callback);
                }
                else{
                    // put all additional params in one string, separator '\0', terminator '\00'
                    var strMessage = "";
                    for(var i = 2; i < arguments.length; i++) {
                        strMessage += arguments[i];
                        strMessage += String.fromCharCode(0);
                    }
                    var sMessageLen = strMessage.length * 2; // sizeof(WCHAR) = 2!
                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `. message strings length: ${sMessageLen}`);
                    strMessage += String.fromCharCode(0);

                    // this doesn't works ... TrcWritef() cut at first \u0000
                    // output >>>. message strings: '111<<<
                    //_logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, ". message strings: '" + strMessage + "'.");
                    // put the string in an object and the stringify() does the job!
                    var objMessage = {}; objMessage["message strings"] =  strMessage;
                    // output example >>>. {"message strings":"111\u0000222\u0000333\u0000\u0000"}.<<<
                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `. ${JSON.stringify(objMessage)}.`);

                    //send it
                    this.FRM_RESOLVE_REQUEST.FWFuncID   = 9;              // CCJOURNAL_FW_FUNC_WRITE_MSG_BY_NUMBER_WITH_ARGS
                    this.FRM_RESOLVE_REQUEST.param1     = messageID;
                    this.FRM_RESOLVE_REQUEST.meta1      = ["LONG", 0];
                    this.FRM_RESOLVE_REQUEST.param2     = sMessageLen;
                    this.FRM_RESOLVE_REQUEST.meta2      = ["SHORT", 0];
                    this.FRM_RESOLVE_REQUEST.param3     = strMessage;
                    this.FRM_RESOLVE_REQUEST.meta3      = ["WCHAR", -1];
                    this.FRM_RESOLVE_REQUEST.paramUL    = 1;

                    _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `. request to send: '${JSON.stringify(this.FRM_RESOLVE_REQUEST)}'.`);
                    this.FrmResolve(this.FRM_RESOLVE_REQUEST, callback);
                }
            }
            else {
                // if user wants callback ...
                if(callback) {
                    // emulate CCJournalFW return code for this case!
                    callback(1);   // CCJOURNAL_FW_ERROR
                }
            }

            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< JournalService::write");
        },

        /**
         * See {@link Wincor.UI.Service.BaseService#onServicesReady}.
         *
         * Reads the configuration from the registry section "\\Services\\JournalService".
         * Registers to various service events (see {@link Wincor.UI.Service.ViewService#SERVICE_EVENTS} to be able to write journal message at the appropriate time.
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         */
        onServicesReady: function ($super) {
            const self = this;

            return ext.Promises.promise(
                function (resolve, reject) {
                    _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> JournalService::onServicesReady()");

                    const instanceName = Wincor.UI.Service.Provider.ConfigService.configuration.instanceName;
                    Wincor.UI.Service.Provider.ConfigService.getConfiguration(instanceName + "\\Services\\JournalService" , null,
                        function configCallback(config) {
                            _logger.LOG_DATA && _logger.log(_logger.LOG_DATA, `* JournalService::configCallback(${JSON.stringify(config)})`);

                            _logger.LOG_DATA && _logger.log(_logger.LOG_DATA, `* JournalService::configCallback, configDefault=${JSON.stringify(self.configDefault)}`);
                            self.config.journalOffset = self.journalOffsets[instanceName];
                            self.config = jQuery.extend(true, self.config, self.configDefault, config); //...and extend our config with missing default config parameters

                            _logger.LOG_DATA && _logger.log(_logger.LOG_DATA, `* JournalService::configCallback, config=${JSON.stringify(self.config)}`);

                            let viewService = self.serviceProvider.ViewService;

                            //persistantly register on NAVIGATE_SPA to write logs
                            viewService.registerForServiceEvent(viewService.SERVICE_EVENTS.NAVIGATE_SPA, function (data) {
                                _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `* JournalService::callback(), SPA data=${JSON.stringify(data)}`);
                                let journalData = jQuery.extend(true, {viewId: viewService.viewContext.viewID}, data);
                                self.write(self.MSG_NUMBERS.MSG_VIEW_DISPLAY, null, JSON.stringify(journalData), journalData.viewId, journalData.viewKey);
                            }, true);

                            //persistantly register on VIEW_ACTIVATED to write logs (here customers can refer to UI properties within the journal messages)
                            viewService.registerForServiceEvent(viewService.SERVICE_EVENTS.VIEW_ACTIVATED, function () {
                                // welcome fragment activates without viewkey
                                if(viewService.viewContext.viewKey) {
                                    let journalData = {
                                        viewId: viewService.viewContext.viewID,
                                        viewKey: viewService.viewContext.viewKey,
                                        url: viewService.viewContext.viewURL
                                    };
                                    self.write(self.MSG_NUMBERS.MSG_VIEW_ACTIVATED, null, JSON.stringify(journalData), journalData.viewId, journalData.viewKey);
                                }
                            }, true);

                            //persistently register on VIEW_CLOSING to write logs
                            viewService.registerForServiceEvent(viewService.SERVICE_EVENTS.VIEW_CLOSING, function (d) {
                                let privateInput = viewService.viewContext.viewConfig.privateInput;
                                _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `* JournalService::callback(), SPA data=${JSON.stringify(d)}`);
                                let data = Object.assign({}, d);
                                if (privateInput) {
                                    data.resultDetail = "*";
                                }
                                self.write(self.MSG_NUMBERS.MSG_VIEW_RESULT, null, JSON.stringify(data), data.viewId, data.viewKey, data.resultCode, data.resultDetail);
                            }, true);

                            //register for the first(!) VIEW_ACTIVATED (i.e. DISPOSAL_TRIGGER_ONETIME) just to register for the ajaxComplete event.
                            //We can not directly register ajaxComplete here in onServicesReady, because that is too early and will not work, most likely because
                            //jQuery is not a member of the contentDocoument at that point in time, because contentDocument is not yet loaded.
                            let regId = viewService.registerForServiceEvent(viewService.SERVICE_EVENTS.VIEW_ACTIVATED, function (data) {
                                let activeFrameName = window.localStorage.getItem("activeFrameName");
                                let iframeDocument = document.getElementById(activeFrameName).contentDocument;
                                let iframeWindow = document.getElementById(activeFrameName).contentWindow;
                                _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `* JournalService::callback2(), activeFrameName=${activeFrameName}, SPA data=${JSON.stringify(data)}`);
                                // Note:
                                // The content window's jQuery object is not the same as the required one, the required belongs to the services document whereas the
                                // second one belongs to the window of the content frame and there we want to get informed on AJAX completions.
                                if(typeof iframeWindow.jQuery === "function") {
                                    try {
                                        iframeWindow.jQuery(iframeDocument).ajaxComplete(function(event, jqXHR, ajaxOptions) {
                                            let journalMessage = {
                                                url: ajaxOptions.url,
                                                statusText: jqXHR.statusText
                                            };
                                            _logger.LOG_SRVC_DATA && _logger.log(_logger.LOG_SRVC_DATA, `* JournalService::onAjaxComplete() ${JSON.stringify(journalMessage)}`);

                                            self.write(self.MSG_NUMBERS.MSG_AJAX_REQUEST, null, JSON.stringify(journalMessage), viewService.viewContext.viewID, viewService.viewContext.viewKey);
                                        });
                                        viewService.deregisterFromServiceEvent(regId);
                                    } catch(e) {
                                        self.logger.log(self.logger.LOG_SRVC_DATA, `* JournalService::onAjaxComplete couldn't register ${e.message}`);
                                    }
                                }
                            }, true); // register persistent, deregister manually

                            $super().then(resolve);

                            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< JournalService::onServicesReady");
                        }
                    );
                }
            );
        },

        /**
         * See {@link Wincor.UI.Service.PTService#initialize}.
         *
         * Initializes the config object, and the JournalService-specific FRM_RESOLVE_REQUEST parameters, so that the CCJournal module is used.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super(); // Invoke superclass's initialize
    
            /**
             * The offset for the journal numbers, depending on the instance.
             * @example {{GUIAPP: 0, GUIDM: 50, GUISOP: 100}}
             * @type {{GUIAPP: number, GUIDM: number, GUISOP: number}}
             */
            this.journalOffsets = {
                GUIAPP: 0,
                GUIDM: 50,
                GUISOP: 100
            };

            this.configDefault = {
            };
            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "> JournalService::initialize()");

            this.FRM_RESOLVE_REQUEST.service = this.NAME;
            this.FRM_RESOLVE_REQUEST.FWName = "CCJournal";

            _logger.LOG_SRVC_INOUT && _logger.log(_logger.LOG_SRVC_INOUT, "< JournalService::initialize");
        }
    });
    return Wincor.UI.Service.JournalService;
});