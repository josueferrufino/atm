/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

/**
 * The idlepresentation code-behind provides the life-cycle for the <i>idlepresentation</i> view.
 * @module idlepresentation
 * @since 1.0/00
 */
define(['code-behind/baseaggregate', 'vm/IdlePresentationViewModel'], function(base) {
    console.log("AMD:idlepresentation");

    const _logger = Wincor.UI.Service.Provider.LogProvider;

    return /** @alias module:idlepresentation */ base.extend({
        name: "idlepresentation",

        /**
         * Instantiates the {@link Wincor.UI.Content.IdlePresentationViewModel} viewmodel.
         * @see {@link module:baseaggregate.activate}.
         * @lifecycle view
         * @return {Promise} resolved when activation is ready.
         */
        activate: function() {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "* | VIEW_AGGREGATE idlepresentation:activate");
            base.container.add(new Wincor.UI.Content.IdlePresentationViewModel(), "flexMain");
            return base.activate();
        },

        /**
         * Overridden to disable a possible <i>txnBackground</i> style.
         * @see {@link module:baseaggregate.compositionComplete}.
         * @lifecycle view
         */
        compositionComplete: function(view, parent) {
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, "* | VIEW_AGGREGATE idlepresentation:compositionComplete");
            base.compositionComplete(view, parent);
            base.container.whenActivationStarted().then(() => {
                base.jQuery('#txnBackground').css("display", "none");
                base.jQuery('div[data-view-id]').attr("data-txn-background", "false");
                // code your specific stuff here
            });
        }
    });
});

