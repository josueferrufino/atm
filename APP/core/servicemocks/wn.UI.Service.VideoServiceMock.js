/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["jquery", "extensions", "servicemocks/wn.UI.Service.BaseServiceMock"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.VideoServiceMock");

    /**
     * The VideoServiceMock simulates the video service.
     * @class
     */
    Wincor.UI.Service.VideoServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock/**@lends Wincor.UI.Service.VideoServiceMock.prototype*/, {

        /**
         * The logical name of this service as used in the service-provider
         * @const
         * @type {string}
         * @default "VideoService"
         */
        NAME: "VideoService",
    
        /**
         * This method is called by the {@link Wincor.UI.Service.Provider#propagateError} if an error occurred in any service. It logs the error to the console.
         *
         *
         * @param {String} serviceName  The name of this service.
         * @param {String} errorType    As defined in {@link Wincor.UI.Service.BaseService#ERROR_TYPE}.
         */
        onError: function(serviceName, errorType) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> VideoServiceMock::onError(" + serviceName + ", " + errorType + ")");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoServiceMock::onError");
        },

        /**
         * This method does nothing in moment.
         *
         * @param {number} step             in %, Range: [1-100], Default: 10 (10%)
         * @param {function=} callback      Reference to a function receiving the return code as a parameter.
         */
        increaseVolume: function(step, callback) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> VideoService::increaseVolume(step:" + step +", ...)");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoService::increaseVolume");
        },

        /**
         * This method does nothing in moment.
         *
         * @param {number} step             in %, Range: [1-100], Default: 10 (10%)
         * @param {function=} callback      Reference to a function receiving the return code as a parameter.
         */
        decreaseVolume: function(step, callback) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> VideoService::decreaseVolume(step:" + step +", ...)");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoService::decreaseVolume");
        },

        /**
         * This method does nothing in moment.
         *
         * @param {function=} callback      Reference to a function receiving the return code as a parameter.
         */
        mute: function(callback) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> VideoService::mute(...)");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoService::mute");
        },

        /**
         * This method does nothing in moment.
         *
         * @param {function=} callback      Reference to a function receiving the return code as a parameter.
         */
        unmute: function(callback) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> VideoService::unmute(...)");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoService::unmute");
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         *
         * @param {object} message      See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         * @returns {Promise}
         * @lifecycle service
         */
        onSetup: function(message) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, `> VideoServiceMock::onSetup('${JSON.stringify(message)}')`);
            return ext.Promises.promise(function(resolve, reject) {
                resolve();
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoServiceMock::onSetup");
            }.bind(this));
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onServicesReady}
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         */
        onServicesReady: function($super) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> VideoServiceMock::onServicesReady()");
            return ext.Promises.promise(function(resolve, reject) {
                $super().then(resolve);
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoServiceMock::onServicesReady");
            }.bind(this));
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#initialize}.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super();
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> VideoServiceMock::initialize()");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< VideoServiceMock::initialize");
        }

    });

    return Wincor.UI.Service.VideoServiceMock;
});
