/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["jquery", "extensions", "servicemocks/wn.UI.Service.BaseServiceMock"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.ConfigServiceMock");

    let _configData = {};

    /**
     * The ConfigServiceMock provides access to the JSON based configuration.
     * @class
     * @since 1.2/00
     */
    Wincor.UI.Service.ConfigServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock/**@lends Wincor.UI.Service.ConfigServiceMock.prototype*/, {

        /**
         * "ConfigServiceMock" - the logical name of this service as used in the service-provider
         * @const
         * @type {string}
         */
        NAME: "ConfigService",

        /**
         * Holds the configuration for the ConfigService, currently only the instance name.
         * @property {Object} configuration
         * @property {String} configuration.instanceName the name of the instance e.g. <i>'GUIAPP', 'GUIDM' or 'GUIVIDEO'</i>
         */
        configuration: {instanceName: ""},

        /**
         * This method is called by the {@link Wincor.UI.Service.Provider#propagateError} if an error occurred in any service. It logs the error to the console.
         *
         *
         * @param {String} serviceName  The name of this service.
         * @param {String} errorType    As defined in {@link Wincor.UI.Service.BaseService#ERROR_TYPE}.
         */
        onError: function(serviceName, errorType) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, `> ConfigServiceMock::onError(${serviceName}, ${errorType})`);
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< ConfigServiceMock::onError");
        },

        /**
         * Gets a configuration from the central config source, e.g. Windows registry.
         *
         * For examples see also {@link Wincor.UI.Service.ConfigService#getConfiguration}.
         *
         * @param {string} section                  Name e.g. "GUIAPP\\Services\\Timeouts"
         * @param {Array<string> | string} keyArray E.g. ["DEFAULT_PAGE_TIMEOUT", "DEFAULT_INPUT_TIMEOUT", ...] or null for requesting the whole section
         * @param {function=} callback              Callback function, which receives the result. See {@link Wincor.UI.Service.ConfigService#translateResponse} for the result structure.
         */
        getConfiguration: function(section, keyArray, callback) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, `> ConfigServiceMock::getConfiguration(${section})`);
            if (callback) {
                setTimeout(() => {
                    const result = {};
                    const path = section.split("\\");
                    let ref;

                    if (section === "") {
                        ref = _configData;
                    }

                    ref = ref || path.reduce((last, act) => {
                        return last ? last[act] : null;
                    }, _configData);

                    if(ref) {
                        keyArray = keyArray || Object.keys(ref);
                        keyArray.forEach((key) => {
                            result[key] = typeof ref[key] !== "undefined" ? ref[key] : null;
                        });
                    }
                    callback(result);
                }, this.responseTimeSimulation);
            }
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< ConfigServiceMock::getConfiguration");
        },

        /**
         * @see {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         *
         * Reads 'UIConfig.json' and 'UIConfigCustom.json', to have the intial "Registry" parameters for the following getConfiguration() calls.
         *
         * @param {object} message      Not used by this service.
         * @returns {Promise}
         * @lifecycle service
         */
        onSetup: function(message) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, `> ConfigServiceMock::onSetup('${JSON.stringify(message)}')`);
            const self = this;
            const fileUIConfig = "../servicemocks/mockdata/UIConfig.json";
            const fileUIConfigCustom = "../servicemocks/mockdata/UIConfig.json";
            return ext.Promises.promise(function(resolve, reject) {
                ext.Promises.Promise.all([self.retrieveJSONData(fileUIConfig), self.retrieveJSONData(fileUIConfigCustom)]).then(dataArray=> {
                    delete dataArray[1]["//"]; // remove possible comment
                    // we need deep merge here, therefore using jquery instead of Object.assign
                    _configData = jQuery.extend(true, {}, dataArray[0], dataArray[1]); // standard props with custom specific ones
                    resolve();
                    self.logger.log(self.logger.LOG_SRVC_INOUT, "< ConfigServiceMock::onSetup");
                }).catch(e=> {
                    console.error(`* importReference error getting ${fileUIConfig} or ${fileUIConfigCustom}`);
                    reject(e);
                });
            });
        },

        /**
         * @see {@link Wincor.UI.Service.BaseServiceMock#onServicesReady}
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         */
        onServicesReady: function($super) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> ConfigServiceMock::onServicesReady()");
            return ext.Promises.promise(function(resolve) {
                $super().then(resolve);
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< ConfigServiceMock::onServicesReady");
            }.bind(this));
        },

        /**
         * @see {@link Wincor.UI.Service.BaseServiceMock#initialize}.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super();
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> ConfigServiceMock::initialize()");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< ConfigServiceMock::initialize");
        }

    });

    return Wincor.UI.Service.ConfigServiceMock;
});
