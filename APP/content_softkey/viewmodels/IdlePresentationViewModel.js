/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

define(["knockout", "vm/CardAnimationsViewModel"], function() {
    "use strict";

    const IDLE_STD_BUTTONS = ['BUTTON_IDLE_LOOP_RIGHT', 'BUTTON_IDLE_LOOP_LEFT'];

    const _logger = Wincor.UI.Service.Provider.LogProvider;
    
    const _localizeService = Wincor.UI.Service.Provider.LocalizeService;
    
    /**
     * IdlePresentationViewModel deriving from {@link Wincor.UI.Content.CardAnimationsViewModel} class. <BR>
     * This viewmodel handles the idlepresentation view.
     * @class
     */
    Wincor.UI.Content.IdlePresentationViewModel = Class.create(Wincor.UI.Content.CardAnimationsViewModel /** @lends Wincor.UI.Content.IdlePresentationViewModel.prototype */, {
    
        /**
         * The timer id for setting the default language.
         * @default -1
         */
        timerId: -1,

        /**
         * Initializes the DOM-associated objects.
         * Overrides {@link Wincor.UI.Content.CardAnimationsViewModel#observe}
         * @param {function=} $super            Reference to the corresponding function of the base class.
         * @param {string} observableAreaId     The area id to observe via knockout
         * @lifecycle viewmodel
         */
        observe: function($super, observableAreaId) {
            _logger.log(_logger.LOG_ANALYSE, `> IdlePresentationViewModel::observe(${observableAreaId})`);
            $super(observableAreaId);
            if(this.designMode) {
                this.viewFlexCardEject(false);
                this.cmdRepos.whenAvailable(IDLE_STD_BUTTONS).then(() => this.cmdRepos.setActive(IDLE_STD_BUTTONS, true));
            }
            _logger.LOG_INOUT && _logger.log(_logger.LOG_INOUT, "< IdlePresentationViewModel::observe");
        },
    
        /**
         * Overridden to get informed when an arbitrary view model send an event.
         * We want to get informed when the "LANGUAGE_CHANGE_REQUESTED" has been occurred by the user.
         * Usually when the popup for language selection has been used to do this.
         * @param {string} id the event id
         * @param {object=} data the event data, usually a JSON notated object, the VM which overrides this method
         * has to know which type of event data are expected dependent by the id of the event.
         * @eventhandler
         */
        onViewModelEvent: function(id, data) {
            _logger.log(_logger.LOG_ANALYSE, `> IdlePresentationViewModel::onViewModelEvent id=${id}`);
            if(id === "LANGUAGE_CHANGE_REQUESTED" && !this.designMode) {
                _localizeService.registerForServiceEvent(_localizeService.SERVICE_EVENTS.LANGUAGE_CHANGED,
                    () => {
                        this.vmHelper.clearTimer(this.timerId);
                        let timeoutVal = parseInt(this.viewConfig.config && this.viewConfig.config.backToDefaultLanguageTimeout ?
                            this.viewConfig.config.backToDefaultLanguageTimeout : 30000);
                        this.timerId = this.vmHelper.startTimer(timeoutVal).onTimeout(() => {
                            _logger.log(_logger.LOG_INFO, `. IdlePresentationViewModel::onViewModelEvent call to set back language to default=${_localizeService.getLanguageMapping().defaultLanguage}`);
                            _localizeService.setLanguage(_localizeService.getLanguageMapping().defaultLanguage);
                        });
                    },
                    _localizeService.DISPOSAL_TRIGGER_ONETIME);
            }
        }
    });
});
