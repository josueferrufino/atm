/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
define(["knockout", "vm/MessageViewModel"], function (ko) {
    "use strict";

    /**
     * Deriving from {@link Wincor.UI.Content.MessageViewModel} class. <BR>
     * This viewmodel provides specific buttons for the adaadjustment view to increase/ decrease the volume and rate.
     * @class
     */
    Wincor.UI.Content.AdaAdjustmentViewModel = Class.create(Wincor.UI.Content.MessageViewModel/** @lends Wincor.UI.Content.AdaAdjustmentViewModel.prototype */, {


        /**
         * Initializes the DOM-associated objects.
         * Overrides {@link Wincor.UI.Content.MessageViewModel#observe}
         * @param {function=} $super            Reference to the corresponding function of the base class.
         * @param {string} observableAreaId     The area id to observe via knockout
         * @lifecycle viewmodel
         */
        observe: function ($super, observableAreaId) {
            try {
                this.logger.log(this.logger.LOG_ANALYSE, "> AdaAdjustmentViewModel::observe(" + observableAreaId + ")");
                $super(observableAreaId);
                this.logger.log(this.logger.LOG_INOUT, "< AdaAdjustmentViewModel::observe");
            }
            catch (e) {
                this.logger.error(e);
            }
        },


        /**
         * Overrides {@link Wincor.UI.Content.BaseViewModel#onButtonPressed}
         * in order to handle certain commands for the adaadjustment view.
         * <p>
         * Handles the following on button pressed actions:<br>
         * <ul>
         *     <li>VOL_MINUS</li>
         *     <li>VOL_PLUS</li>
         *     <li>RATE_MINUS</li>
         *     <li>RATE_PLUS</li>
         * </ul>
         * </p>
         * @param {function} $super the super function
         * @param {string} id the id of the command that was triggered
         * @eventhandler
         */
        onButtonPressed: function ($super, id) {
            if (!this.designMode) {
                //alert(id);

                if( id === "VOL_MINUS" ){
                    this.serviceProvider.AdaService.decreaseVolume( null );
                }
                else if( id === "VOL_PLUS" ){
                    this.serviceProvider.AdaService.increaseVolume( null );
                }
                else if( id === "RATE_MINUS" ){
                    this.serviceProvider.AdaService.decreaseRate( null );
                }
                else if( id === "RATE_PLUS" ){
                    this.serviceProvider.AdaService.increaseRate( null );
                }
                else{
                    $super(id);
                }
            }
            else { // design mode
                if(id !== "VOL_MINUS" && id !== "VOL_PLUS" && id !== "RATE_MINUS" && id !== "RATE_PLUS") {
                    $super(id);
                }
            }
        }

    });
});
