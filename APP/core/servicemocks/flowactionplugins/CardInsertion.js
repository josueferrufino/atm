/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */

/**
 * @module flowactionplugins/CardInsertion
 */
define(["extensions"], function(ext) {
    "use strict";
    console.log("AMD:CardInsertion");

    const _eventService = Wincor.UI.Service.Provider.EventService;
    const _dataService = Wincor.UI.Service.Provider.DataService;
    const PROP_CARD_ACTIVE = _dataService.getKey("PROP_CARD_ACTIVE");
    const EVENT_INFO = _eventService.getEventInfo("TRANSACTION_MODULE");

    return {

        /**
         * Emulates a business logic flow action when running: Sends an event when card insert 'HALFTIMEOUT'.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // function: the current view key (corresponding this this plug-in name)</li>
         * <li>config          // object: Configuration of Config.js</li>
         * <li>container       // object: ViewModelContainer</li>
         * <li>serviceProvider // object: a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved when the action is ready or rejected on any error.
         */
        onActivateReady: function(context) {
            return ext.Promises.promise((resolve, reject) => {
                try {
                    ext.Promises.Promise
                        .delay(5000).then(() => {
                            if(context.currentViewKey() === "CardInsertion") { // check whether we are in card insert still
                                _eventService.onEvent({
                                    FWName: EVENT_INFO.NAME,
                                    FWEventID: EVENT_INFO.ID_ESCALATION,
                                    FWEventParam: "STARTED"
                                });
                            } else {
                                resolve();
                            }
                        })
                        .delay(5000).then(() => {
                            if(context.currentViewKey() === "CardInsertion") { // check whether we are in card insert still
                                _eventService.onEvent({
                                    FWName: EVENT_INFO.NAME,
                                    FWEventID: EVENT_INFO.ID_ESCALATION,
                                    FWEventParam: "HALFTIMEOUT"
                                });
                            } else {
                                resolve();
                            }
                        })
                        .delay(5000).then(() => {
                            if(context.currentViewKey() === "CardInsertion") { // check whether we are in card insert still
                                resolve(context.action = "cardNotInserted");
                            } else {
                                resolve();
                            }
                    });
                } catch(e) {
                    reject("FlowAction plug-in CardInsertion::onActivateReady has been failed " + e);
                }
            });
        },

        /**
         * Emulates a hardware event action when running: Sends an event when card insert or DIP insert on contact less reader.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // function: the current view key (corresponding this this plug-in name)</li>
         * <li>config          // object: Configuration of Config.js</li>
         * <li>action          // the current action (hardware trigger action)</li>
         * <li>container       // object: ViewModelContainer</li>
         * <li>serviceProvider // object: a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved when the action is ready or rejected on any error.
         */
        onHardwareEvent: function(context) {
            return ext.Promises.promise((resolve, reject) => {
                try {
                    if(context.action === "cardInsert" || context.action === "contactlessReaderArea") {
                        _dataService.setValues(PROP_CARD_ACTIVE, "1", null);
                        _eventService.onEvent({
                            FWName: EVENT_INFO.NAME,
                            FWEventID: EVENT_INFO.ID_CARD,
                            FWEventParam: "INSERT"
                        });
                        setTimeout(() => resolve(context.action), 500); // hardware running time for insert card or DIP
                    } else {
                        resolve(context.action);
                    }
                } catch(e) {
                    reject("FlowAction plug-in CardInsertion::onHardwareEvent has been failed " + e);
                }
            });
        },

        /**
         * Emulates a business logic flow action: Sends an event for EPP ETS layout, if corresponding property 'PROP_ETS_LAYOUT' is set.
         * @param {Object} context is:<br>
         * <ul>
         * <li>currentViewKey  // the current view key (corresponding this this plug-in name)</li>
         * <li>currentFlow     // the current flow</li>
         * <li>action          // the current action (customer selection)</li>
         * <li>config          // Configuration of Config.js</li>
         * <li>container       // ViewModelContainer</li>
         * <li>serviceProvider // a service provider reference</li>
         * </ul>
         * @returns {Promise} gets resolved (may argument a new view key destination) when the action is ready or rejected on any error.
         */
        onGuiResult: function(context) {
            return ext.Promises.promise((resolve, reject) => {
                try {
                    resolve();
                } catch(e) {
                    reject("FlowAction plug-in CardInsertion::onGuiResult has been failed " + e);
                }
            });
        }
    };
});
