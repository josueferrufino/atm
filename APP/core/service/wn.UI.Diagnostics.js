/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["wn-ui"],function(){return console.log("AMD:wn.UI.Diagnostics"),Wincor.UI.Diagnostics={},Wincor.UI.Diagnostics});