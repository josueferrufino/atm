/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["jquery", "knockout", "extensions", "servicemocks/wn.UI.Service.BaseServiceMock"], function(jQuery, ko, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.DataServiceMock");
    
    /**
     *
     * @type {Wincor.UI.Diagnostics.LogProvider|*|Wincor.UI.Diagnostics.LogProvider|*}
     * @const
     * @private
     */
    const _logger = Wincor.UI.Diagnostics.LogProvider;
    
    const LISE_DATA_SEPARATOR = "_||_";
    const PROP_ARRAY_MARKER = "[A";
    const PROP_INDEX_BEGIN_MARKER = "[";
    const PROP_INDEX_END_MARKER = "]";


    let _businessData = {};
    let _businessPropertyKeys = {};
    let _localizeService;
    let _viewService;
    let _configService;
    let _controlPanelService;

    /**
     * Array containing elements of {@link Wincor.UI.Service.DataServiceMock#DataRegistration}.
     * @type {Array}
     */
    let _dataRegistrators = []; // [DataRegistration1, DataRegistration2, ... ]

    let _usedBusinessProperties = new Map();
    let _allBusinessProperties = new Map();
    let _mapAllProps = false;

    function extractStartIndexFromArrayKey(key) {
        let startIdx = 0;
        if (key && key.indexOf(PROP_ARRAY_MARKER) !== -1) {
            let accessData = key.substring(key.indexOf(PROP_INDEX_BEGIN_MARKER) + 1, key.indexOf(PROP_INDEX_END_MARKER));
            accessData = accessData.split(",");
            startIdx = parseInt(accessData[1]);
        }
        return startIdx;
    }
    
    /**
     * The DataServiceMock provides access to the JSON based business data file.
     * @class
     * @since 1.2/00
     */
    Wincor.UI.Service.DataServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock/**@lends Wincor.UI.Service.DataServiceMock.prototype*/, {

        /**
         * The logical name of this service as used in the {@link Wincor.UI.Service.Provider}.
         * @default DataService
         * @const
         * @type {string}
         */
        NAME: "DataService",

        /**
         * Contains the business property keymap.
         * @type {Object}
         */
        businessData: _businessData,

        /**
         * Contains the UI specific property keys.
         * @type {Object}
         */
        UIPropertyKeys: {},

        /**
         * Structure containing the registered data keys
         * @class
         */
        DataRegistration: function() {
            /**
             * the registered data keys of the business properties.
             * @type {Array<string>}
             */
            this.keys = [];          //the registered dataKeys

            /**
             * A callback function which is called when the dataChanged event is triggered
             * @type {function}
             */
            this.onUpdate = null;    //this callback is called when the dataChanged event is triggered

            /**
             * If registration is persistent (true), a content-page unload will not remove the registration, false otherwise.
             * @type {boolean}
             */
            this.persistent = false; //if registration is persistent, a content-page unload will not remove the registration
        },

        /**
         * The service events for the DataServiceMock.
         * @enum {string}
         */
        SERVICE_EVENTS: {
            /**
             * Fired when any setValues() has been done.
             * @event  Wincor.UI.Service.DataServiceMock#SERVICE_EVENTS:DATA_CHANGED
             * @eventtype service
             */
            DATA_CHANGED: "DATA_CHANGED"
        },
    
        /**
         * This event is sent if there are missing data keys.
         * @type {Object}
         * @eventtype native
         * @event  Wincor.UI.Service.ViewService#EVENT_MISSING_DATA_KEYS TODO: hier ViewService#EVENT... oder ViewServiceMock#EVENT... ?
         */
        EVENT_MISSING_DATA_KEYS: null,

        /**
         * Either maps all business properties or only the currently used ones.
         * The method considers (if the argument is true) the business property map,
         * the properties within the localized text and within the view mappings.
         * In 2017 this number is round about 150 properties so far.
         * @param {boolean} all true means all, else used only
         */
        mapAllProperties: function(all) {
            _mapAllProps = all;
            if(all) {
                let props, value;
                // property/key mapping
                let keyMappedProps = new Set();
                props = Object.keys(_businessPropertyKeys);
                for(let i = props.length - 1; i >= 0; i--) {
                    // filter out special properties only used by the business logic
                    if(_businessPropertyKeys[props[i]].indexOf(".") === -1) {
                        keyMappedProps.add(_businessPropertyKeys[props[i]]);
                    }
                }
                // text properties
                let textProps = _localizeService.getUsedProperties();
                // view mapping properties
                let viewMappingProps = _viewService.getUsedProperties();
                // all property sets iterating
                let allSets = [keyMappedProps, textProps, viewMappingProps];
                for(let i = allSets.length - 1; i >= 0; i--) {
                    for(let key of allSets[i]) {
                        let parts = this.extractKeyPartsFromProperty(key);
                        value = this.getPropValue(parts, _businessData);
                        if(value !== null && typeof value === "object") {
                            value = JSON.stringify(value);
                        } else if(value === null) {
                            continue;
                        }
                        _allBusinessProperties.set(parts.key, value);
                    }
                }
                _controlPanelService.updateBusinessProperties(_allBusinessProperties);
            } else{
                _controlPanelService.updateBusinessProperties(_usedBusinessProperties);
            }
        },

        /**
         * Clean the {@link Wincor.UI.Service.DataService#dataArray} containing all
         * {@link Wincor.UI.Service.DataService#DataRegistration} structures which has been set by the
         * {@link Wincor.UI.Service.DataService#getValues} method.
         */
        cleanDataRegistrations: function() {
            _logger.log(_logger.LOG_SRVC_INOUT, "> DataServiceMock::cleanDataRegistrations()");
            var newDataArray = [];
            // keep the persistent items in -newDataArray
            for(let i = _dataRegistrators.length - 1; i >= 0; i--) {
                if(_dataRegistrators[i].persistent) {
                    // keep the persistent item
                    newDataArray.push(_dataRegistrators[i]);
                }
            }
            _dataRegistrators = newDataArray; // empty array or the persistent items kept
            _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::cleanDataRegistrations");
        },
    
        /**
         * Gets the current data registrations.
         * Data registrations are properties which for an update callback has been given via {@link Wincor.UI.Service.DataService#getValues}
         * @return {Array<DataRegistration>} the current data registrations
         */
        getDataRegistrations: function() {
            return _dataRegistrators;
        },
        
        /**
         * Gets a business property key from the business property key map.
         * @param {String} key the to get the property
         * @returns {String} the property key or an empty string in case of error
         */
        getKey: function(key) {
            if(key in _businessPropertyKeys) {
                return _businessPropertyKeys[key];
            } else {
                console.error(`${key} not in business property key map available.`);
                return "";
            }
        },

        /**
         * Get the values of the requested parameters stored in the registry.
         * @param {Array<string> | string} keys e.g. ["VAR_MY_HTML_NAME_S", "CUSTOMER_SURNAME", ...]
         * @param {function (Object)=} callback
         * @param {function (Object)} onUpdateCallback callback is called when a key was updated.
         * @param {boolean=} persistent true, if the -onUpdateCallback callback function should stay persistent, even a
         *        {@link Wincor.UI.Service.DataService#cleanDataRegistrations} is invoked, false otherwise.
         */
        getValues: function(keys, callback, onUpdateCallback, persistent) {
            _logger.log(_logger.LOG_SRVC_INOUT, `> DataServiceMock::getValues(${keys})`);
            const self = this;
            keys = Array.isArray(keys) ? keys : [keys];
            
            function sendMessageMissingKeys(missingKeys) {
                if (missingKeys.length > 0) {
                    self.EVENT_MISSING_DATA_KEYS.viewID = _viewService.viewContext.viewID;
                    self.EVENT_MISSING_DATA_KEYS.viewKey = _viewService.viewContext.viewKey;
                    self.EVENT_MISSING_DATA_KEYS.keys = missingKeys;
                    self.sendEvent(self.EVENT_MISSING_DATA_KEYS);
                }
            }

            function generateSpecificResponse() {
                let response = {};
                let uniqueKeys = [];
                let missingKeys = [];
                for(let i = 0; i < keys.length; ++i) { // must use forward loop, because of order of the keys (e.g. ProFlex4Op)
                    let key = keys[i];

                    if(key === void 0 || key === null) {
                        _logger.error(`Error: DataServiceMock::getValues invalid key detected: ${key}`);
                        continue;
                    }
                    if(!uniqueKeys.includes(key)) {
                        uniqueKeys.push(key);
                        // check for something like "CCCHCCDMTAFW_CHEQUE_ACCEPTED[idx] or CCCHCCDMTAFW_CHEQUE_ACCEPTED.id[idx]"
                        // except for something like "CCTAFW_PROP_EMV_APPLICATION_SELECTION_LISE_DATA[A,0,2]" which is for LISE concept
                        let parts = self.extractKeyPartsFromProperty(key);
                        let index = parts.idx === -1 ? 0 : parts.idx;
                        let attrChain = parts.attrChain;
                        let value = _businessData[parts.key];
                        let viewKey = _viewService.viewContext.viewKey;
                        // NOTE: Empty string is a valid value, if("") delivers false always
                        if(value || value === "") {
                            // first lookup with concrete view key, if it not exist then lookup with '*'
                            if(!value[viewKey] && value !== "") {
                                viewKey = "*";
                                value = value[viewKey];
                                if(Array.isArray(value)) { // value is an array ?
                                    value = value[index];
                                }
                                if(attrChain) { // value is expected as JSON
                                    value = self.getValueFromJSON(value, attrChain);
                                }
                                if(!value && value !== "") {
                                    value = null;
                                    _logger.error(`Warning: No business value for key=${key} and viewKey=${viewKey} available. Please check 'BusinessData.json' file`);
                                }
                            } else if(typeof value === "object") { // if a specific view key has been succeeded, its value is expected as object then...
                                let data = value[viewKey];
                                if(data) {
                                    value = data;
                                    if(Array.isArray(value)) { // value is an array ?
                                        value = value[index];
                                    }
                                    if(attrChain) { // value is expected as JSON
                                        value = self.getValueFromJSON(value, attrChain);
                                    }
                                } else {
                                    value = null;
                                    _logger.error(`No LISE business value for key=${key} and viewKey=${viewKey} available. Please check 'BusinessData.json' file`);
                                }
                            } else {
                                value = null;
                                _logger.error(`No LISE business value for key=${key} and viewKey=${viewKey} available. Value is not an object as it was expected. Please check 'BusinessData.json' file`);
                            }
                        } else {
                            value = null;
                            missingKeys.push(key);
                            _logger.error(`Warning: No business value for key=${key} and viewKey=${viewKey} available. Please check 'BusinessData.json' file`);
                        }
                        // LISE handling necessary ?
                        // e.g.: If we got a key like "CCTAFW_PROP_PREPAID_LISE_LIST[A,7,11]"
                        if(value && typeof value === "string" && value.indexOf(LISE_DATA_SEPARATOR) !== -1) { // we expect a string here due to indexOf
                            // - Code when must consider the startIndex -
                            // We have to extract the start index and deliver data from that point of array:
                            // let dataArray = value.split(LISE_DATA_SEPARATOR), startIdx = 0;
                            // if(key.indexOf(PROP_ARRAY_MARKER) !== -1) {
                            //     startIdx = extractStartIndexFromArrayKey(key);
                            // }

                            // - Legacy code when not consider the startIndex -
                            // We have to ignore the start index and deliver always the whole data length.
                            // This is because the retrieved data (which are the original generated tooling data) are handled as net data, where the startIndex has already been
                            // considered by a lower-level data generator (e.g. DataDictionaryExt) and hence must not be considered once again.
                            response[key] = value.split(LISE_DATA_SEPARATOR);
                        } else {
                            response[key] = value; // a normal key/value
                            if(key.indexOf(PROP_ARRAY_MARKER) !== -1) { // LISE access prop with only one item?
                                response[key] = [value];
                            }
                        }
                    } else {
                        _logger.error(`Warning: DataServiceMock::getValues double key detected: ${key}`);
                    }
                }
                sendMessageMissingKeys(missingKeys);
                // handle business properties map
                let props = Object.keys(response);
                for(let i = 0; i < props.length; i++) {
                    if(!missingKeys.includes(props[i])) {
                        _usedBusinessProperties.set(props[i], response[props[i]]);
                        _allBusinessProperties.set(props[i], response[props[i]]);
                    }
                }
                _controlPanelService.updateBusinessProperties(_mapAllProps ? _allBusinessProperties : _usedBusinessProperties);

                return response;
            }

            //build a new DataRegistration object and add it to our Array if (and only if!) an onUpdateCallback is given
            if(onUpdateCallback) {
                let dataReg = new this.DataRegistration();
                dataReg.keys = keys;
                dataReg.onUpdate = onUpdateCallback;
                dataReg.persistent = persistent ? persistent : false; // note, persistent arg is optional
                _dataRegistrators.push(dataReg);
            }

            if(callback) {
                window.setTimeout(() => callback(generateSpecificResponse()), this.responseTimeSimulation);
            }
            _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::getValues");
        },

        /**
         * Set the values of the requested parameters to be stored within the business logic.
         * @param {Array<string> | string} keys e.g. a single string or ["VAR_MY_HTML_NAME_S", "CUSTOMER_SURNAME", ...]
         * @param {Array<string> | string} values e.g. a single string or ["cardinsert.html", "Doe", ...]
         * @param {function=} callback is called when the values are set
         */
        setValues: function(keys, values, callback) {
            _logger.log(_logger.LOG_SRVC_INOUT, `> DataServiceMock::setValues(${keys}, ${values})`);

            keys = Array.isArray(keys) ? keys : [keys];
            values = Array.isArray(values) ? values : [values];

            let errorMessage;
            if(keys.length !== values.length) {
                errorMessage = "DataServiceMock.setValues(): keys and values do not have the same size.";
            } else {
                for(let i = 0; i < keys.length; ++i) {
                    if(keys[i] === void 0 || keys[i] === null) { //do NOT use '!keys[i]' because empty string is okay, 0 is okay
                        errorMessage = "DataServiceMock.setValues(): keys contains null or undefined.";
                        break;
                    }
                    if(values[i] === void 0 || values[i] === null) { //do NOT use '!keys[i]' because empty string is okay, 0 is okay
                        errorMessage = "DataServiceMock.setValues(): values contains null or undefined.";
                        break;
                    }
                }
            }

            if(errorMessage) {
                _logger.error(errorMessage);
                Wincor.UI.Service.Provider.propagateError("DataService::setValues", this.ERROR_TYPE.REQUEST);
            } else { // update data:
                // Note:
                // Business data are context dependent. They are stored like this:
                // CCTAFW_PROP_GENERIC_LIST_SELECTION_JSON_GROUPS: { "*": "", "ReceiptPreferenceMultipleChoiceSelection": {...}}.
                // Whereas a simple view state property like VAR_VOUCHER_VIEWSTATE_S: { "*": "0" } is usually context independent.
                // Because the extended design mode has only a simple business logic simulation the data are
                // stored either view key dependent or independent.
                // To be in the right context several properties are stored view key specific.
                // For example the property "CCTAFW_PROP_GENERIC_LIST_SELECTION_JSON_GROUPS" is a generic property, but the values are context
                // dependent which is a specific view key like "ReceiptPreferenceMultipleChoiceSelection".
                // If the setting would be generic, a getValues call for another context would get the data from a setting before, which would be
                // wrong in such a case.
                for(let i = 0; i < keys.length; ++i) {
                    let viewKey = _viewService.viewContext.viewKey;
                    let key = keys[i];
                    if(key in _businessData) {
                        // value exist with the current view key? (in such a case the value has been red with the same view key before)
                        // or store unspecific to allow access view key independent "*"
                        _businessData[key][_businessData[key][viewKey] ? viewKey : "*"] = values[i];
                        _usedBusinessProperties.set(key, values[i]);
                        _allBusinessProperties.set(key, values[i]);
                    } else { // key maybe contain an index with ...[0] which is often the case for indexed based properties or even a JSON attribute access notation
                        // check for something like "CCCHCCDMTAFW_CHEQUE_ACCEPTED[idx] or CCCHCCDMTAFW_CHEQUE_ACCEPTED.id[idx]"
                        // don't accept something like "CCTAFW_PROP_EMV_APPLICATION_SELECTION_LISE_DATA[A,0,2]" which is for LISE concept
                        let parts = this.extractKeyPartsFromProperty(key);
                        if(parts.index !== -1 || parts.attrChain) {
                            key = parts.key;
                            if(key in _businessData) {
                                // value exist with the current view key? (in such a case the value has been red with the same view key before)
                                // or store unspecific to allow access view key independent "*"
                                viewKey = _businessData[key][viewKey] ? viewKey : "*";
                                if(_businessData[key][viewKey]) {
                                    let attrChain = parts.attrChain;
                                    // ControlPanel->properties tab: In order to not store every kind (CCTAFW_PROP_CURRENCY[0], CCTAFW_PROP_CURRENCY[999],
                                    // CCTAFW_PROP_CURRENCY.id[0]) of property access we use the plain key name (CCTAFW_PROP_CURRENCY) always, because
                                    // otherwise we can't easily update the list of properties (referencing the same raw value)
                                    // when updated a value using the plain (CCTAFW_PROP_CURRENCY) or any other access (CCTAFW_PROP_CURRENCY[999], etc.) method.
                                    // Otherwise its possible using the original name by uncommenting this line and setting the -name instead of the -key:
                                    // let name = this.buildKeyFromParts(parts);
                                    if(Array.isArray(_businessData[key][viewKey])) {
                                        let index = parts.idx;
                                        if(attrChain) { // value is expected as JSON
                                            let candidate = _businessData[key][viewKey][index];
                                            _businessData[key][viewKey][index] = this.setValueFromJSON(candidate, attrChain, values[i]);
                                            if(_businessData[key][viewKey][index] !== candidate) { // do we have successfully updated?
                                                _usedBusinessProperties.set(key, values[i]);
                                                _allBusinessProperties.set(key, values[i]);
                                            }
                                        } else {
                                            _businessData[key][viewKey][index] = values[i];
                                            _usedBusinessProperties.set(key, values[i]);
                                            _allBusinessProperties.set(key, values[i]);
                                        }
                                    } else {
                                        if(attrChain) { // value is expected as JSON
                                            let candidate = _businessData[key][viewKey];
                                            _businessData[key][viewKey] = this.setValueFromJSON(candidate, attrChain, values[i]);
                                            if(_businessData[key][viewKey] !== candidate) { // do we have successfully updated?
                                                _usedBusinessProperties.set(key, values[i]);
                                                _allBusinessProperties.set(key, values[i]);
                                            }
                                        } else {
                                            _businessData[key][viewKey] = values[i];
                                            _usedBusinessProperties.set(key, values[i]);
                                            _allBusinessProperties.set(key, values[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                this.fireServiceEvent(this.SERVICE_EVENTS.DATA_CHANGED, {keys: keys, values: values});
                _controlPanelService.updateBusinessProperties(_mapAllProps ? _allBusinessProperties : _usedBusinessProperties);
            }
            this.callbackCaller(callback);
            _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::setValues");
        },

        /**
         * Updates the given key array with the corresponding values array.
         * @param {Array<string> | String} keyArray either an array or a plain string with the property name(s)
         * @param {Array<string>  | String} valueArray either an array or a plain string with the value(s)
         * @param {function} callback
         */
        updateValues: function(keyArray, valueArray, callback) {
            _logger.log(_logger.LOG_SRVC_INOUT, `> DataServiceMock::updateValues(${keyArray}, ${valueArray})`);
            // we set the properties before we call the updates
            this.setValues(keyArray, valueArray, callback);
            if(!Array.isArray(keyArray)) {
                keyArray = [keyArray];
            }
            if(!Array.isArray(valueArray)) {
                valueArray = [valueArray];
            }
            for(let k = 0; k < keyArray.length; k++) {
                let key = keyArray[k];
                for(let i = 0; i < _dataRegistrators.length; i++) {
                    let keys = _dataRegistrators[i].keys;
                    for(let j = 0; j < keys.length; j++) {
                        if(keys[j] === key) {
                            _logger.log(_logger.LOG_ANALYSE, `. DataServiceMock::updateValues found property=${key} has been updated with value=${valueArray[k]}`);
                            let result = {};  //build the object
                            result[keys[j]] = valueArray[k];
                            _dataRegistrators[i].onUpdate(result);     //call the callback with the object
                        }
                    }
                }
            }
            _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::updateValues");
        },

        /**
         * This method is called by the service-provider if an error occurred in any service
         * @eventhandler
         * @param {String} serviceName
         * @param {String} errorType
         */
        onError: function(serviceName, errorType) {
            _logger.log(_logger.LOG_SRVC_INOUT, `> DataServiceMock::onError(${serviceName}, ${errorType})`);
            _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::onError");
        },
    
        /**
         * This will read (again) all data from the JSON files.
         * This was originally done in onSetup, but now moved into a separate function, to be called anytime.
         *
         * @returns {Promise}
         */
        updateJSONData: async function() {
            _logger.log(_logger.LOG_SRVC_INOUT, "> DataServiceMock::updateJSONData()");
            let [profile, profileExtension] = await this.getToolingProfile();
            const fileUIKeyMap = "../servicedata/UIPropertyKeyMap.json";
            const fileBusinessData = `../servicemocks/mockdata/BusinessData${profileExtension}.json`;
            const fileBusinessDataCustom = "../servicemocks/mockdata/BusinessDataCustom.json";
            const fileBusinessKeys = "../servicedata/BusinessPropertyKeyMap.json";
            const fileBusinessCustomKeys = "../servicedata/BusinessPropertyCustomKeyMap.json";
    
            let dataArray = await ext.Promises.Promise.all([
                this.retrieveJSONData(fileBusinessData),
                this.retrieveJSONData(fileBusinessDataCustom),
                this.retrieveJSONData(fileBusinessKeys),
                this.retrieveJSONData(fileBusinessCustomKeys),
                this.retrieveJSONData(fileUIKeyMap)
            ]);
            try {
                delete dataArray[1]["//"]; // remove possible comment
                _businessData = Object.assign(dataArray[0], dataArray[1]); // standard props with custom specific ones
                delete dataArray[3]["//"]; // remove possible comment
                _businessPropertyKeys = Object.assign(dataArray[2], dataArray[3]); // standard keys with custom specific ones
                this.UIPropertyKeys = dataArray[4];
                this.businessData = _businessData;
                _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::updateJSONData");
            } catch(e) {
                console.error(`* importReference error getting ${fileBusinessData}, ${fileBusinessKeys}, ${fileBusinessDataCustom} or ${fileBusinessCustomKeys}`);
                throw(e);
            }
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         * In addition it will call _updateJSONData_ to read the JSON files.
         *
         * @param {object} message      See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         * @returns {Promise}
         * @lifecycle service
         */
        onSetup: function(message) {
            _logger.log(_logger.LOG_SRVC_INOUT, `> DataServiceMock::onSetup('${JSON.stringify(message)}')`);
            return ext.Promises.promise((resolve, reject) => {
                this.updateJSONData()
                    .then(() => {
                        _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::onSetup");
                        resolve();
                    })
                    .catch(e => {
                        reject(e);
                    });
            });
        },

        /**
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         * @see {@link Wincor.UI.Service.BaseServiceMock#onServicesReady}
         */
        onServicesReady: function($super) {
            _logger.log(_logger.LOG_SRVC_INOUT, "> DataServiceMock::onServicesReady()");
            return ext.Promises.promise(resolve => {
                _controlPanelService = this.serviceProvider.ControlPanelService;
                _localizeService = this.serviceProvider.LocalizeService;
                _configService = this.serviceProvider.ConfigService;
                _viewService = this.serviceProvider.ViewService;
                _viewService.registerForServiceEvent(_viewService.SERVICE_EVENTS.VIEW_CLOSING, this.cleanDataRegistrations.bind(this), true);
                _viewService.registerForServiceEvent(_viewService.SERVICE_EVENTS.SHUTDOWN, this.cleanDataRegistrations.bind(this), true);
                $super().then(resolve);
                _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::onServicesReady");
            });
        },

        /**
         * This function can be called from outside the UI to retrieve arbitrary viewModel data as a property!
         * The functionality relies on a naming convention regarding the attribute name and context to access.
         * Naming convention is defined as follows:
         * GUIINSTANCENAME_ATTRIBUTENAME_CONTEXTNAME, where "GUIINSTANCENAME_" will be cut by gui.dll and not arrive here!
         * This automatic value resolution via naming convention heavily relies on a correct case-sensitive spelling, therefore mappings can be
         * used within the file "core/servicedata/businessPropertyKeyMap.json"
         * For CONTEXTNAME the "observableAreaId" of a corresponding viewModel can be given so that a valid name could be:
         * "GUIAPP_flexHeader.date". Service attributes can also be accessed if exposed via proxy using the servicename as context like:
         * "ViewService.viewContext.viewConfig"
         * The result will be send back to the native part as response containing the stringified value of the attribute or null if it does not exist.
         * @param {object} message message containing request-data
         * @param {string} message.propertyName contains the propertyName - has to follow the above naming convention
         * @param {string} message.propertyValue will be set to the value of the property, unchanged if not found...
         * @return {string} value for internal requests
         */
        getPropertyString: function(message) {
            const _logger = this.logger;
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `> DataServiceMock::getPropertyString(${JSON.stringify(message)})`);
            let name = message.propertyName || "";
            let value = null;
            let ret;
            // filter probably instance name from prop key
            if(name.startsWith(_configService.configuration.instanceName)) {
                name = name.substr(_configService.configuration.instanceName.length + 1);
            }
            // try to get it from mapping, otherwise try to disassemble directly
            name = this.UIPropertyKeys[name] || name;
            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `. after mapping: ${name}`);
            if(!name) {
                _logger.error("DataServiceMock::getPropertyString argument 'message' is invalid: Expect an object with at least attribute 'propertyName' !");
                return "";
            }

            // now we have: flexHeader.date -> first index is context, next attributes might be more than one for nested objects... walk structure hierarchy
            let parts = name.split(".");
            let contextName = parts.splice(0, 1)[0]; // pop first entry as context
            let context = Wincor.UI.Service.Provider[contextName];

            if (!context) {
                // it wasn't a service, look for specials
                if(contextName.indexOf("Wincor") === 0) {
                    context = Wincor;
                } else if(contextName.indexOf("window") === 0) {
                    context = window.frames[0];
                }
            }

            // If there is a viewset switch active, we won't have "Content" available!
            if (!context && Wincor.UI.Content && Wincor.UI.Content.ViewModelContainer) {
                // at last try if there is an observable area / vm with this name...
                context = Wincor.UI.Content.ViewModelContainer.getById(contextName);
            }

            if (context) {
                value = parts.reduce(function(c, a) {
                    if (c && a in c) {
                        try {
                            if(typeof c[a] === "function" && "__ko_proto__" in c[a]) {
                                return c[a]();
                            } else {
                                return c[a];
                            }
                        } catch(e) {
                            _logger.error(`DataServiceMock::getPropertyString exception during attribute evaluation: '${e.message}'`);
                        }
                    }
                    return void 0;
                }, context);
            }

            if (value !== void 0 && value !== null) {
                message.propertyValue = value.toString();
                ret = this.REQUEST_RESPONSE_OK;
            } else {
                let err = name;
                if (message.propertyName !== name) {
                    err = `${message.propertyName}' aka '${name}` ;
                }
                _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `DataServiceMock::getPropertyString attribute could not be found for: '${err}'`);
                ret = "-1";
            }

            if (typeof value === "object") {
                try {
                    value = JSON.stringify(value);
                } catch(e) {
                    value = value.toString();
                }
            }

            _logger.LOG_DETAIL && _logger.log(_logger.LOG_DETAIL, `< DataServiceMock::getPropertyString returns ${ret} data: ${JSON.stringify(message)}`);
            return value;
        },

        /**
         * Initializes the member of this class.
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         * @see {@link Wincor.UI.Service.BaseServiceMock#initialize}.
         */
        initialize: function($super) {
            $super();
            this.DYNAMIC_PROPERTY_PREFIX = "GUIAPP_GENERIC_PROP_"; // will be set when instance-name is available. (e.g. "GUIAPP_GENERIC_PROP_")
            _logger.log(_logger.LOG_SRVC_INOUT, "> DataServiceMock::initialize()");
            this.EVENT_MISSING_DATA_KEYS = Object.assign(Object.assign({}, this.EVENT), {
                service: this.NAME,
                viewID: -1,
                viewKey: null,
                eventName: "MissingDataKeys",
                keys: [],
            });
            
            _logger.log(_logger.LOG_SRVC_INOUT, "< DataServiceMock::initialize");
        }

    });

    return Wincor.UI.Service.DataServiceMock;
});
