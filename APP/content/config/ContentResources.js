/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
console.log('__ContentResources__');

const _config = {
  requireConfig: {
    //By default load any module IDs from GUI
    skipDataMain: true,
    baseUrl: '../',
    priority: 'ContentResources',
    paths: {
      jquery: '../lib/jquery-min',
      'jquery-ui': '../lib/jquery-ui-min',
      knockout: '../lib/knockout',
      bluebird: '../lib/internal/wn.UI.bluebird.enhanced',
      //-- durandal
      text: '../lib/text',
      durandal: '../lib/durandal/js',
      plugins: '../lib/durandal/js/plugins',
      transitions: '../lib/durandal/js/transitions',
      //--
      lib: '../lib',
      'json-validator': '../lib/ajv.min',
      'wn-ui': '../core/service/wn.UI',
      extensions: '../lib/internal/wn.UI.extensions',
      'code-behind': 'views/script',
      vm: 'viewmodels',
      'vm-util': 'viewmodels/base',
      prototype: '../lib/class.system',
      'ko-mapping': '../lib/knockout.mapping',
      'ko-custom': 'viewmodels/base/ko.CustomUtils',
      'ui-content': 'viewmodels/base/wn.UI.Content',
      basevm: 'viewmodels/base/BaseViewModel',
      'vm-container': 'viewmodels/base/ViewModelContainer',
      flexuimapping: 'viewmodels/base/ProFlex4Op',
      config: '../content/config'
    },

    shim: {
      prototype: {
        // Don't actually need to use this object as
        // Prototype affects native objects and creates global ones too
        // but it's the most sensible object to return
        exports: 'Prototype',
        init: function() {
          return Prototype;
        }
      },
      jquery: {
        exports: 'jQuery'
      },
      'jquery-ui': ['jquery'],
      knockout: {
        //These script dependencies should be loaded before loading
        //knockout.js
        deps: ['jquery']
      }
    }
  }
};

requirejs.config(_config.requireConfig);

/**
 * The module ContentResources is the overall starting point for the UI front-end (content part).
 * <p>
 * It starts the SPA application, requires, among other, some DurandalJS specific modules.
 * Furthermore the AMD configuration for the content is done here as well as the definition for the module aliases.
 * </p>
 * @module ContentResources
 * @since 1.0/00
 */
define(['ui-content'], function(content) {
  'use strict';
  console.log('AMD:ContentResources');
  // ensure config is ready before we start up
  require(['config/Config'], function(config) {
    config.ready.then(() => {
      require([
        'bluebird',
        'durandal/system',
        'durandal/app',
        'durandal/viewLocator',
        'plugins/router',
        'lib/internal/wn.UI.DurandalExtensions',
        'vm-container',
        'vm-util/ViewModelHelper',
        'basevm'
      ], function(
        bluebird,
        system,
        app,
        viewLocator,
        router,
        durandalExtensions
      ) {
        content.Config = config;
        //>>excludeStart("build", true);
        system.debug(false);
        //>>excludeEnd("build");

        app.title = 'ProFlex4 UI';

        app.configurePlugins({
          router: true
        });

        durandalExtensions.initialize();

        app.start().then(() => {
          viewLocator.useConvention(config.modulesPath, config.viewsPath);
          //Replace 'viewmodels' in the moduleId with 'views' to locate the view.
          //Look for partial views in a 'views' folder in the root.
          //Show the app by setting the root view model for our application with a transition.
          app.setRoot(config.modulesPath + config.startModule);
        });
      });
    });
  });
});
