/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


*/
define(["jquery", "extensions", "servicemocks/wn.UI.Service.BaseServiceMock"], function(jQuery, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.BeepServiceMock");

    const _audio = new Audio();

    /**
     * The BeepServiceMock handles simulates a SEL framework regarding the beep handling.
     * @class
     */
    Wincor.UI.Service.BeepServiceMock = Class.create(Wincor.UI.Service.BaseServiceMock/** @lends Wincor.UI.Service.BeepServiceMock.prototype*/, {

        /**
         * The logical name of this service as used in the service-provider
         * @const
         * @type {string}
         * @default "BeepService"
         */
        NAME: "BeepService",

        /**
         * This flag turns the beeping ON/OFF.
         * @type {boolean}
         * @default true
         */
        beeping: true,

        /**
         * This is the code for the beep of inactive EPP keys.
         * Possible values are:<br>
         *   <ul>
         *     <li>0  = OFF</li>
         *     <li>2  = SIU_KEYPRESS</li>
         *     <li>4  = SIU_EXCLAMATION</li>
         *     <li>8  = SIU_WARNING</li>
         *     <li>16 = SIU_ERROR</li>
         *     <li>32 = SIU_CRITICAL</li>
         *   </ul>
         * @type {number}
         * @default 8
         */
        beepInactiveKeyCode: 8,
    
        /**
         * This method is called by the {@link Wincor.UI.Service.Provider#propagateError} if an error occurred in any service. It logs the error to the console.
         *
         *
         * @param {String} serviceName  The name of this service.
         * @param {String} errorType    As defined in {@link Wincor.UI.Service.BaseService#ERROR_TYPE}.
         */
        onError: function(serviceName, errorType) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, `> BeepServiceMock::onError(${serviceName}, ${errorType})`);
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< BeepServiceMock::onError");
        },

        /**
         * Do a beep.<br>
         * This method is playing two sounds. For code 2 beep.wav, and for code 8 beep-warning.wav.
         *
         * @param {number} [code=2]     beep type, possibilities:
         * <ul>
         *      <li>2  (CCSELFW_SIU_KEYPRESS)</li>
         *      <li>4  (CCSELFW_SIU_EXCLAMATION)</li>
         *      <li>8  (CCSELFW_SIU_WARNING)</li>
         *      <li>16 (CCSELFW_SIU_ERROR)</li>
         *      <li>32 (CCSELFW_SIU_CRITICAL)</li>
         * </ul>
         * @param {function} [callback=null]      Reference to a function receiving the return code as a parameter.
         */
        beep: function(code = 2, callback = null) {
            !arguments[2] && this.logger.log(this.logger.LOG_SRVC_INOUT, `> BeepServiceMock::beep(code:${code})`);
            if(localStorage.getItem("activateBeepServiceOn") === "true") {
                switch(code) {
                    case 2:
                        if(_audio.paused) {
                            _audio.setAttribute("src", "assets/beep.wav");
                        } else {
                            setTimeout(() => {
                                this.beep(code, null, true);
                            }, 50);
                            !arguments[2] && this.logger.log(this.logger.LOG_SRVC_INOUT, "< BeepServiceMock::beep");
                            return;
                        }
                        break;
                    case 8:
                        if(_audio.paused) {
                            _audio.setAttribute("src", "assets/beep-warning.wav");
                        } else {
                            setTimeout(() => {
                                this.beep(code, null, true);
                            }, 50);
                            !arguments[2] && this.logger.log(this.logger.LOG_SRVC_INOUT, "< BeepServiceMock::beep");
                            return;
                        }
                        break;
                }
                try {
                    _audio.play();
                } catch(e) {
                    // nothing here for work, error maybe thrown id the play will be interrupted
                }
                this.callbackCaller(callback);
            }
            !arguments[2] && this.logger.log(this.logger.LOG_SRVC_INOUT, "< BeepServiceMock::beep");
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         *
         * @param {Object} message      See {@link Wincor.UI.Service.BaseServiceMock#onSetup}
         * @returns {Promise}
         * @lifecycle service
         */
        onSetup: function(message) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, `> BeepServiceMock::onSetup('${JSON.stringify(message)}')`);
            return ext.Promises.promise(resolve => {
                resolve();
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< BeepServiceMock::onSetup");
            });
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#onServicesReady}
         *
         * @param {function=} $super     Reference to the corresponding function of the base class.
         * @returns {Promise}
         * @lifecycle service
         */
        onServicesReady: function($super) {
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> BeepServiceMock::onServicesReady()");
            return ext.Promises.promise(resolve => {
                $super().then(resolve);
                this.logger.log(this.logger.LOG_SRVC_INOUT, "< BeepServiceMock::onServicesReady");
            });
        },

        /**
         * See {@link Wincor.UI.Service.BaseServiceMock#initialize}.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function($super) {
            $super();
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> BeepServiceMock::initialize()");
            _audio.setAttribute("src", "assets/beep.wav");
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< BeepServiceMock::initialize");
        }

    });

    return Wincor.UI.Service.BeepServiceMock;
});
