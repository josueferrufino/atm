/**
 @preserve
 Copyright (c) 2001-2018 by Wincor Nixdorf International GmbH,
 Heinz-Nixdorf-Ring 1, 33106 Paderborn, Germany

 This software is the confidential and proprietary information
 of Wincor Nixdorf.
 You shall not disclose such Confidential Information and shall
 use it only in accordance with the terms of the license agreement
 you entered into with Wincor Nixdorf.


 */
define(["base-service", "extensions"], function(baseSvc, ext) {
    "use strict";
    console.log("AMD:wn.UI.Service.PTService");

    let crossCallId = 0;

    /**
     * The PTService is used as base-class for services that need to directly call business-logic functions.
     *
     * @class
     * @abstract
     */
    Wincor.UI.Service.PTService = Class.create(Wincor.UI.Service.BaseService/**@lends Wincor.UI.Service.PTService.prototype*/, {

        /**
         * The logical name of this service as used in the service-provider
         *
         * @const
         * @type {string}
         * @default "PTService"
         */
        NAME: "PTService",

        /**
         * Not used!
         *
         * @private
         */
        FRM_EVENT: null,

        /**
         * Protopas request object, extends {@link Wincor.UI.Gateway#REQUEST},
         * with Protopas function FrmResolve arguments.
         *
         * @type {object}
         */
        FRM_RESOLVE_REQUEST: null,

        /**
         * The Extended message literal providing Protopas specific data.
         * The methodName value for a sync request.
         *
         * @const
         * @type {string}
         * @default "FrmResolve"
         */
        METHOD_FRM_RESOLVE: "FrmResolve",

        /**
         * Protopas request object, extends {@link Wincor.UI.Gateway#REQUEST},
         * with Protopas function FrmAsyncResolve arguments.
         *
         * @type {object}
         */
        FRM_ASYNC_RESOLVE_REQUEST: null,

        /**
         * The Extended message literal providing Protopas specific data.
         * The methodName value for an async request.
         *
         * @const
         * @type {string}
         * @default "FrmAsyncResolve"
         */
        METHOD_FRM_ASYNC_RESOLVE: "FrmAsyncResolve",

        /**
         * The Extended message literal providing Protopas specific data.
         * The methodName value for a general event.
         *
         * @const
         * @type {string}
         * @default "OnFrmEvent"
         */
        EVENT_ON_FRM_EVENT: "OnFrmEvent",

        /**
         * Object containing the definitions of supported meta data types for Protopas FrmResolve calls.
         *
         * @enum {object}
         */
        META_TYPE: {
            /**
             * Defines the meta data type as ANSI coded CHAR(s).
             *
             * @const
             * @type {string}
             * @default "CHAR_ANSI"
             */
            CHAR_ANSI: "CHAR_ANSI",

            /**
             * Defines the meta data type as list of CHAR_ANSI.
             *
             * @const
             * @type {string}
             * @default "PARLIST_ANSI"
             */
            PARLIST_ANSI: "PARLIST_ANSI",

            /**
             * Defines the meta data type as UTF8 coded CHAR(s).
             *
             * @const
             * @type {string}
             * @default "CHAR_UTF8"
             */
            CHAR_UTF8: "CHAR_UTF8",

            /**
             * Defines the meta data type as list of CHAR_UTF8.
             *
             * @const
             * @type {string}
             * @default "PARLIST_UTF8"
             */
            PARLIST_UTF8: "PARLIST_UTF8",

            /**
             * Defines the meta data type as UTF16 coded WCHAR(s).
             *
             * @const
             * @type {string}
             * @default "WCHAR"
             */
            WCHAR: "WCHAR",

            /**
             * Defines the meta data type as HEX coded Byte(s).
             * Byte 0 is HEX coded to "00" ... Byte 255 is HEX coded to "FF".
             *
             * @const
             * @type {string}
             * @default "HEX"
             */
            HEX: "HEX",

            /**
             * Defines the meta data type as SHORT.
             *
             * @const
             * @type {string}
             * @default "SHORT"
             */
            SHORT: "SHORT",

            /**
             * Defines the meta data type as USHORT.
             *
             * @const
             * @type {string}
             * @default "USHORT"
             */
            USHORT: "USHORT",

            /**
             * Defines the meta data type as LONG.
             *
             * @const
             * @type {string}
             * @default "LONG"
             */
            LONG: "LONG",

            /**
             * Defines the meta data type as ULONG.
             *
             * @const
             * @type {string}
             * @default "ULONG"
             */
            ULONG: "ULONG",

            /**
             * Defines the meta data type as NULL pointer.
             *
             * @const
             * @type {string}
             * @default "NULL"
             */
            NULL: "NULL"
        },

        /**
         * Sends the request to the Protopas bus.
         * See {@link Wincor.UI.Service.BaseService#sendRequest}
         *
         * @param {object}    request       Request that will be send to the gateway.
         * @param {function=} callback      Reference to a function receiving the return code as a parameter.
         */
        FrmResolve: function (request, callback) {
            //Wincor.UI.Diagnostics.LogProvider.log("Wincor.UI.Service.PTService(FrmResolve):"); // Request to send: '" + JSON.stringify(request) + "'.");
            var message = Object.assign(Object.assign({}, this.FRM_RESOLVE_REQUEST), request || {});

            this.sendRequest(message, callback);
        },

        /**
         * Allows cross-calling functions of services hosted in other UI instances
         * source/target function has to be callable internally and externally and crossCall has to be configured via interface definition file
         *
         * @private
         * @param {string} targetInstance
         * @param {string} functionName
         * @param {Array} args Default is []
         * @param {string=} serviceName the service name. Default is SERVICE_NAME
         * @param {{}=} extraData
         * @returns {Promise}
         */
        crossCall: function(targetInstance, functionName, args = [], serviceName = this.NAME, extraData={}) {
            crossCallId++;

            const instanceName = this.serviceProvider.getInstanceName();

            // this is the message to be passed
            const serviceRequest = {
                service: serviceName,
                type: "request",
                methodName: functionName,
                args: args,
                caller: instanceName,
                acknowledgeId: '',
                extraData: Object.assign({}, extraData),
                result: ""
            };
            serviceRequest.acknowledgeId = `${instanceName}->${targetInstance}.${this.NAME}::${functionName}_crossCall_${crossCallId}`;

            const req = Object.assign({}, Wincor.UI.Gateway.prototype.REQUEST);
            req.service = this.NAME;
            req.methodName = "FrmResolve";
            req.FWName = targetInstance;
            req.FWFuncID = 60;
            req.param1 = JSON.stringify(serviceRequest);
            req.meta1 = [this.META_TYPE.CHAR_UTF8, -1];
            req.param2 = "";
            req.meta2 = ["NULL", 0];
            req.param3 = "";
            req.meta3 = ["NULL", 0];
            req.param4 = "";
            req.meta4 = ["NULL", 0];
            req.param5 = "";
            req.meta5 = ["NULL", 0];
            req.paramUL = 0;
            req.RC = -1;

            this.logger.LOG_SRVC_INOUT && this.logger.log(this.logger.LOG_SRVC_INOUT, `> PTService::crossCall(${JSON.stringify(req, null, " ")})\nccid=${serviceRequest.acknowledgeId}`);

            return ext.Promises.promise((resolve, reject) => {
                this.eventMap.set(serviceRequest.acknowledgeId, (message) => {
                    this.logger.LOG_SRVC_INOUT && this.logger.log(this.logger.LOG_SRVC_INOUT, `< PTService::crossCall returns ${JSON.stringify(message, null, " ")}\nccid=${serviceRequest.acknowledgeId}`);
                    resolve(message.result);
                    this.eventMap.delete(serviceRequest.acknowledgeId);
                });
                this.FrmResolve(req, (result) => {
                    if (result.RC !== 0) {
                        this.eventMap.delete(serviceRequest.acknowledgeId);
                        reject(result.RC);
                        this.logger.LOG_ERROR && this.logger.log(this.logger.LOG_ERROR, `< PTService::crossCall returned ${result.RC})\nccid=${serviceRequest.acknowledgeId}`);
                    }
                });
            });
        },

        /**
         * Acknowledge function for cross-calling functions of services hosted in other UI instances
         * @private
         * @param {string} targetInstance
         * @param {string} targetService
         * @param {string} ackId
         * @param {*} result json stringify compatible result
         * @param {{}=} extraData Default is {}
         * @returns {Promise}
         */
        crossCallAcknowledge: function(targetInstance, targetService, ackId, result, extraData = {}) {

            return ext.Promises.promise((resolve, reject) => {

                const ackEvent = {
                    service: targetService,
                    type: "event",
                    eventName: ackId,
                    result: result,
                    extraData: Object.assign({}, extraData)
                };

                const req = Object.assign({}, Wincor.UI.Gateway.prototype.REQUEST);
                req.service = this.NAME;
                req.methodName = "FrmResolve";
                req.FWName = targetInstance;
                req.FWFuncID = 60;
                req.param1 = JSON.stringify(ackEvent);
                req.meta1 = [this.META_TYPE.CHAR_UTF8, -1];
                req.param2 = "";
                req.meta2 = ["NULL", 0];
                req.param3 = "";
                req.meta3 = ["NULL", 0];
                req.param4 = "";
                req.meta4 = ["NULL", 0];
                req.param5 = "";
                req.meta5 = ["NULL", 0];
                req.paramUL = 0;
                req.RC = -1;

                this.logger.LOG_SRVC_INOUT && this.logger.log(this.logger.LOG_SRVC_INOUT, `> PTService::crossCallAcknowledge(${JSON.stringify(req, null, " ")})\nccid=${ackId}`);

                this.FrmResolve(req, (result) => {
                    this.logger.LOG_SRVC_INOUT && this.logger.log(this.logger.LOG_SRVC_INOUT, `< PTService::crossCallAcknowledge returns ${JSON.stringify(result, null, " ")}\nccid=${ackId}`);
                    if (result.RC === 0) {
                        resolve();
                    } else {
                        reject(`PTService::crossCallAcknowledge failed with RC!=0 -> ${JSON.stringify(result)}`)
                    }
                });
            });
        },

        /**
         * Allows cross-instance eventing for services hosted in other UI instances
         * @private
         * @param {string} targetInstance
         * @param {string} targetService
         * @param {string} eventId
         * @param {*} eventData json stringify compatible new attribute value
         * @returns {Promise}
         */
        crossCallEvent: function(targetInstance, targetService, eventId, eventData) {
            crossCallId++;

            const instanceName = this.serviceProvider.getInstanceName();
            const crossCallEvent = {
                service: targetService,
                type: "event",
                eventName: eventId,
                caller: instanceName,
                acknowledgeId: ``,
                eventData: eventData
            };
            crossCallEvent.acknowledgeId = `${instanceName}->${targetInstance}.${this.NAME}:${eventId}_crossCallEvent_${crossCallId}`;

            const req = Object.assign({}, Wincor.UI.Gateway.prototype.REQUEST);
            req.service = this.NAME;
            req.methodName = "FrmResolve";
            req.FWName = targetInstance;
            req.FWFuncID = 60;
            req.param1 = JSON.stringify(crossCallEvent);
            req.meta1 = [this.META_TYPE.CHAR_UTF8, -1];
            req.param2 = "";
            req.meta2 = ["NULL", 0];
            req.param3 = "";
            req.meta3 = ["NULL", 0];
            req.param4 = "";
            req.meta4 = ["NULL", 0];
            req.param5 = "";
            req.meta5 = ["NULL", 0];
            req.paramUL = 0;
            req.RC = -1;

            this.logger.LOG_SRVC_INOUT && this.logger.log(this.logger.LOG_SRVC_INOUT, `> PTService::crossCallEvent(${JSON.stringify(req, null, " ")})\nccid=${crossCallEvent.acknowledgeId}`);

            return ext.Promises.promise((resolve, reject) => {

                this.eventMap.set(crossCallEvent.acknowledgeId, (message) => {
                    this.logger.LOG_SRVC_INOUT && this.logger.log(this.logger.LOG_SRVC_INOUT, `< PTService::crossCallEvent returns ${JSON.stringify(message, null, " ")}\nccid=${crossCallEvent.acknowledgeId}`);
                    resolve(message.result);
                    this.eventMap.delete(crossCallEvent.acknowledgeId);
                });

                this.FrmResolve(req, (result) => {
                    if (result.RC !== 0) {
                        this.logger.LOG_ERROR && this.logger.log(this.logger.LOG_ERROR, `< PTService::crossCallEvent returned ${result.RC})\nccid=${crossCallEvent.acknowledgeId}`);
                        reject(`PTService::crossCallEvent failed with RC!=0 -> ${JSON.stringify(result)}`)
                    }
                });
            });
        },

        /**
         * See {@link Wincor.UI.Service.BaseService#initialize}.
         * The member {@link Wincor.UI.Service.PTService#FRM_RESOLVE_REQUEST} is initialised.
         * The member {@link Wincor.UI.Service.PTService#FRM_ASYNC_RESOLVE_REQUEST} is initialised.
         *
         * @param {function=} $super    Reference to the corresponding function of the base class.
         * @lifecycle service
         */
        initialize: function ($super) {
            $super(); // Invoke superclass's initialize
            this.logger.log(this.logger.LOG_SRVC_INOUT, "> PTService::initialize()");
            this.FRM_RESOLVE_REQUEST = Object.assign(Object.assign({}, this.REQUEST), {
                service: this.NAME,
                methodName: this.METHOD_FRM_RESOLVE,
                FWName: "",
                FWFuncID: 0,
                param1: "",
                meta1: [],
                param2: "",
                meta2: [],
                param3: "",
                meta3: [],
                param4: "",
                meta4: ["NULL", 0],
                param5: "",
                meta5: ["NULL", 0],
                paramUL: 0,
                RC: -1 //TODO: remove - should be returned in standard "result" element -> O.N.: if removed the values[] arrays should also removed from the getXXX methods in the other services ...
            });
            this.FRM_ASYNC_RESOLVE_REQUEST = Object.assign(Object.assign({}, this.REQUEST), {
                service: this.NAME,
                methodName: this.METHOD_FRM_ASYNC_RESOLVE,
                FWName: "",
                FWFuncID: 0,
                param1: "",
                meta1: [],
                param2: "",
                meta2: [],
                param3: "",
                meta3: [],
                param4: "",
                meta4: ["NULL", 0],
                param5: "",
                meta5: ["NULL", 0],
                paramUL: 0,
                RC: -1 //TODO: remove - should be returned in standard "result" element -> O.N.: if removed the values[] arrays should also removed from the getXXX methods in the other services ...
            });
            this.logger.log(this.logger.LOG_SRVC_INOUT, "< PTService::initialize");
        }
    });
    return Wincor.UI.Service.PTService;
});